$("#create-workspace-form").validate({
    rules: {
        name: {
            required: true,
        },
        type: {
            required: true,
        }
    },
    errorElement: 'small',
    errorPlacement: function(error, element) {
        var placement = $(element).data('error');
        if (placement) {
            $(placement).append(error)
        } else {
            error.addClass('text-danger');
            error.insertAfter(element);
        }
    }
});
