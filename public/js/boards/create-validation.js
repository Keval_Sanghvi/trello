$("#create-board-form").validate({
    rules: {
        title: {
            required: true,
        },
        background: {
            required: true,
        },
        visibility: {
            required: true,
        }
    }
});
