<?php

namespace App\Console;

use App\Models\Board;
use App\Models\Card;
use App\Models\Lists;
use App\Models\User;
use App\Notifications\CardDueDate;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Log;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            $cards = Card::where('due_date', '<', Carbon::now()->addDays(1))->get();
            foreach($cards as $card) {
                foreach($card->users as $user) {
                    $user = User::find($user->id);
                    $user->notify(new CardDueDate($card, Board::findOrFail(Lists::findOrFail($card->list_id)->board_id)));
                }
            }
        })->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
