<?php

namespace App\Listeners;

use App\Events\BoardUpdateEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class BoardUpdateEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BoardUpdateEvent  $event
     * @return void
     */
    public function handle(BoardUpdateEvent $event)
    {
        //
    }
}
