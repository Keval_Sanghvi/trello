<?php

namespace App\Listeners;

use App\Events\ListUpdateEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class ListUpdateEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ListUpdateEvent  $event
     * @return void
     */
    public function handle(ListUpdateEvent $event)
    {
        //
    }
}
