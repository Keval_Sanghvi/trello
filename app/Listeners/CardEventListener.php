<?php

namespace App\Listeners;

use App\Events\CardEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class CardEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CardEvent  $event
     * @return void
     */
    public function handle(CardEvent $event)
    {
        //
    }
}
