<?php

namespace App\Listeners;

use App\Events\ListEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class ListEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ListEvent  $event
     * @return void
     */
    public function handle(ListEvent $event)
    {
        //
    }
}
