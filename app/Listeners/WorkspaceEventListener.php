<?php

namespace App\Listeners;

use App\Events\WorkspaceEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class WorkspaceEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  WorkspaceEvent  $event
     * @return void
     */
    public function handle(WorkspaceEvent $event)
    {
        //
    }
}
