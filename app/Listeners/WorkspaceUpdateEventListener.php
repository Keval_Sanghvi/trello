<?php

namespace App\Listeners;

use App\Events\WorkspaceUpdateEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class WorkspaceUpdateEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  WorkspaceUpdateEvent  $event
     * @return void
     */
    public function handle(WorkspaceUpdateEvent $event)
    {
        //
    }
}
