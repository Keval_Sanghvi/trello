<?php

namespace App\Listeners;

use App\Events\CardUpdateEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class CardUpdateEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CardUpdateEvent  $event
     * @return void
     */
    public function handle(CardUpdateEvent $event)
    {
        //
    }
}
