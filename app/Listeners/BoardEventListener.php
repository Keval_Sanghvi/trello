<?php

namespace App\Listeners;

use App\Events\BoardEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class BoardEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BoardEvent  $event
     * @return void
     */
    public function handle(BoardEvent $event)
    {
        //
    }
}
