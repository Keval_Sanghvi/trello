<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function workspaces()
    {
        return $this->belongsToMany(Workspace::class)->withTimestamps();
    }

    public function boards()
    {
        return $this->belongsToMany(Board::class)->withPivot('favourite');
    }

    public function cards()
    {
        return $this->belongsToMany(Card::class);
    }

    // Accessor
    public function getAvatarAttribute()
    {
        $size = 40;
        $name = $this->name;
        return "https://ui-avatars.com/api/?name={$name}&rounded=true&size={$size}";
    }

    // Accessor
    public function getDefaultWorkspaceNameAttribute()
    {
        $workspace = $this->workspaces()->first();
        return $workspace->name;
    }

    // Model Event
    public static function boot()
    {
        parent::boot();
        static::created(function(User $user) {
            $workspace_name = $user->name . "'s Workspace";
            $workspace = Workspace::create([
                'name' => $workspace_name,
                'type' => 'Other',
                'visibility' => 'private',
            ]);
            $user->workspaces()->attach($workspace->id, ['admin' => 1]);
        });
    }

    public function getAdminStatusForWorkspace(Workspace $workspace)
    {
        return $this->workspaces()->where(['workspace_id' => $workspace->id, 'admin' => 1])->exists() ? 'admin' : 'normal';
    }
}
