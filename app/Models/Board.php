<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Board extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function workspace()
    {
        return $this->belongsTo(Workspace::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function lists()
    {
        return $this->hasMany(Lists::class);
    }

    public function isFavourite()
    {
        if(auth()->user()->boards()->where('boards.id', $this->id)->exists()) {
            return auth()->user()->boards()->where('boards.id', $this->id)->first()->pivot->favourite;
        } else {
            return false;
        }
    }
}
