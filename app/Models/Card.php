<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Card extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function list()
    {
        return $this->belongsTo(Lists::class);
    }

    public function checklists()
    {
        return $this->hasMany(Checklist::class);
    }

    public function getLabels()
    {
        return DB::table('labels')->where('card_id', '=', $this->id)->get();
    }
}
