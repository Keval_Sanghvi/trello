<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\View;
use Auth;

class BaseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function($request, $next) {
            $workspace_types = ['Operations', 'Human_Resources', 'Sales_CRM', 'Marketing', 'Education', 'Small_Businesses', 'Engineering_IT', 'Other'];
            $board_visibility_types = ['private', 'public', 'workspace'];
            $board_backgrounds = ['#0079bf', '#d29034', '#519839', '#b04632', '#89609e', '#cd5a91', '#4bbf6b', '#00aecc', '#838c91'];
            $navbar_backgrounds = ['#014c78', '#a37029', '#3d752a', '#873526', '#5b4069', '#873b60', '#317a45', '#00879e', '#505659'];
            $label_colors = ['#007bff', '#6c757d', '#28a745', '#ffc107', '#dc3545', '#343a40'];
            $workspaces = auth()->user()->workspaces;
            $boards = auth()->user()->boards;
            auth()->user()->unreadNotifications->markAsRead();
            $notifications = auth()->user()->notifications()->get();
            View::share('workspaces', $workspaces);
            View::share('workspace_types', $workspace_types);
            View::share('boards', $boards);
            View::share('notifications', $notifications);
            View::share('board_visibility_types', $board_visibility_types);
            View::share('board_backgrounds', $board_backgrounds);
            View::share('navbar_backgrounds', $navbar_backgrounds);
            View::share('label_colors', $label_colors);
            return $next($request);
        });
    }
}
