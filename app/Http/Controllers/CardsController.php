<?php

namespace App\Http\Controllers;

use App\Events\CardEvent;
use App\Events\CardUpdateEvent;
use App\Models\Board;
use App\Models\Card;
use App\Models\Checklist;
use App\Models\Lists;
use App\Models\User;
use App\Notifications\NewUserAddedToCard;
use Carbon\Carbon;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CardsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $list = Lists::findOrFail($request->list_id);
        $board = Board::findOrFail($request->board_id);
        $this->authorize('update', $board);
        $rules = [
            'name' => 'required|max:255',
        ];
        $this->validate($request, $rules);
        $card = $list->cards()->create([
            'name' => $request->name,
        ]);
        auth()->user()->cards()->attach($card->id, ['created_at' => Carbon::now(), 'updated_at' => Carbon::now()]);
        CardEvent::dispatch(['id' => $board->id, 'name' => $request->name, 'list_id' => $list->id, 'card_id' => $card->id, 'auth' => auth()->id()]);
        return response()->json(['card' => $request->name, 'card_id' => $card->id, 'list_id' => $request->list_id], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Card  $card
     * @return \Illuminate\Http\Response
     */
    public function show(Card $card)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Card  $card
     * @return \Illuminate\Http\Response
     */
    public function edit(Card $card)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Card  $card
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Card $card)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Card  $card
     * @return \Illuminate\Http\Response
     */
    public function destroy(Card $card)
    {
        //
    }

    public function changeList(Request $request)
    {
        $card = Card::findOrFail($request->card_id);
        $card->update([
            'list_id' => $request->list_id,
        ]);
        CardUpdateEvent::dispatch(['id' => $request->board_id, 'name' => $card->name, 'list_id' => $request->list_id, 'card_id' => $card->id, 'auth' => auth()->id(), 'feature' => 'listChange']);
        return response()->json(['card_id' => $request->card_id, 'list_id' => $request->list_id], 200);
    }

    public function getCard(Request $request)
    {
        $card = Card::findOrFail($request->card_id);
        $users = $card->users->take(5);
        $totalUsers = $card->users->count();
        $totalUsers -= $users->count();
        $avatars = [];
        foreach($users as $user) {
            $avatars[] = $user->avatar;
        }
        $list = Lists::findOrFail($card->list_id);
        $checklists = Checklist::where('card_id', $card->id)->get()->toArray();
        $labels = DB::table('labels')->where('card_id', '=', $card->id)->get();
        return response()->json(['card_id' => $request->card_id, 'name' => $card->name, 'description' => $card->description, 'due_date' => $card->due_date, 'status' => $card->status, 'list_name' => $list->name, 'checklists' => $checklists, 'members' => $avatars, 'totalUsers' => $totalUsers, 'labels' => $labels], 200);
    }

    public function changeName(Request $request)
    {
        $card = Card::findOrFail($request->card_id);
        $board = Board::findOrFail($request->board_id);
        $this->authorize('update', $board);
        $rules = [
            'newName' => 'required|max:255',
        ];
        $this->validate($request, $rules);
        if($request->newName == $card->name) {
            return response()->json(['result' => $request->card_id], 200);
        }
        $card->update([
            'name' => $request->newName,
        ]);
        CardUpdateEvent::dispatch(['id' => $request->board_id, 'name' => $card->name, 'list_id' => $request->list_id, 'card_id' => $card->id, 'auth' => auth()->id(), 'feature' => 'nameChange']);
        return response()->json(['result' => $request->card_id], 200);
    }

    public function changeDescription(Request $request)
    {
        $card = Card::findOrFail($request->card_id);
        $board = Board::findOrFail($request->board_id);
        $this->authorize('update', $board);
        $rules = [
            'newDescription' => 'required|max:255',
        ];
        $this->validate($request, $rules);
        if($request->newDescription == $card->description) {
            return response()->json(['result' => $request->card_id], 200);
        }
        $card->update([
            'description' => $request->newDescription,
        ]);
        return response()->json(['result' => $request->card_id], 200);
    }

    public function inviteMembers(Board $board, Card $card, Request $request): RedirectResponse
    {
        $this->authorize('invite', $board);
        $users = $request->validate([
            'card_users' => 'required|array',
        ]);
        foreach($users as $user) {
            $card->users()->attach($user, ['created_at' => Carbon::now(), 'updated_at' => Carbon::now()]);
            $user = User::find($user[0]);
            $user->notify(new NewUserAddedToCard($card, $board));
        }
        return redirect()->back();
    }

    public function changeDueDate(Request $request)
    {
        $card = Card::findOrFail($request->card_id);
        $board = Board::findOrFail($request->board_id);
        $this->authorize('update', $board);
        $rules = [
            'due_date' => 'required',
        ];
        $this->validate($request, $rules);
        $card->update([
            'due_date' => $request->due_date,
        ]);
        return response()->json(['result' => $request->card_id], 200);
    }

    public function changeStatus(Request $request)
    {
        $card = Card::findOrFail($request->card_id);
        $board = Board::findOrFail($request->board_id);
        $this->authorize('update', $board);
        $rules = [
            'status' => 'required',
        ];
        $this->validate($request, $rules);
        $card->update([
            'status' => $request->status,
        ]);
        return response()->json(['result' => $request->card_id], 200);
    }

    public function createLabel(Request $request)
    {
        $card = Card::findOrFail($request->card_id);
        $board = Board::findOrFail($request->board_id);
        $this->authorize('update', $board);
        $rules = [
            'name' => 'required',
            'color' => 'required',
        ];
        $this->validate($request, $rules);
        $label_id = DB::table('labels')->insertGetId(
            ['name' => $request->name, 'color' => $request->color, 'card_id' => $request->card_id, 'board_id' => $request->board_id, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
        );
        CardUpdateEvent::dispatch(['id' => $request->board_id, 'card_id' => $card->id, 'auth' => auth()->id(), 'label_name' => $request->name, 'label_color' => $request->color, 'label_id' => $label_id, 'feature' => 'createLabel']);
        return response()->json(['result' => $label_id], 200);
    }

    public function removeLabel(Request $request)
    {
        $board = Board::findOrFail($request->board_id);
        $this->authorize('update', $board);
        DB::table('labels')->delete($request->label_id);
        CardUpdateEvent::dispatch(['id' => $request->board_id, 'auth' => auth()->id(), 'label_id' => $request->label_id, 'feature' => 'removeLabel']);
        return response()->json(['result' => 'true'], 200);
    }
}
