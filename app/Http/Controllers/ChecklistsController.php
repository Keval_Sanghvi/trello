<?php

namespace App\Http\Controllers;

use App\Models\Board;
use App\Models\Card;
use App\Models\Checklist;
use Illuminate\Http\Request;

class ChecklistsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $board = Board::findOrFail($request->board_id);
        $this->authorize('update', $board);
        $rules = [
            'name' => 'required|max:255',
            'card_id' => 'required',
        ];
        $this->validate($request, $rules);
        $checklist = Checklist::create([
            'name' => $request->name,
            'card_id' => $request->card_id,
        ]);
        return response()->json(['name' => $request->name, 'card_id' => $request->card_id, 'checklist_id' => $checklist->id], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Checklist  $checklist
     * @return \Illuminate\Http\Response
     */
    public function show(Checklist $checklist)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Checklist  $checklist
     * @return \Illuminate\Http\Response
     */
    public function edit(Checklist $checklist)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Checklist  $checklist
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Checklist $checklist)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Checklist  $checklist
     * @return \Illuminate\Http\Response
     */
    public function destroy(Checklist $checklist)
    {
        //
    }

    public function changeStatus(Request $request)
    {
        $board = Board::findOrFail($request->board_id);
        $this->authorize('update', $board);
        $rules = [
            'status' => 'required',
            'checklist_id' => 'required',
        ];
        $this->validate($request, $rules);
        $checklist = Checklist::findOrFail($request->checklist_id);
        $checklist->update([
            'status' => $request->status,
        ]);
        return response()->json(['result' => $request->checklist_id], 200);
    }

    public function remove(Request $request)
    {
        $board = Board::findOrFail($request->board_id);
        $card = Card::findOrFail($request->card_id);
        $this->authorize('update', $board);
        $card->checklists()->delete();
        return response()->json(['result' => $request->card_id], 200);
    }
}
