<?php

namespace App\Http\Controllers;

use App\Events\WorkspaceEvent;
use App\Events\WorkspaceUpdateEvent;
use App\Http\Requests\Workspaces\CreateWorkspaceRequest;
use App\Http\Requests\Workspaces\UpdateWorkspaceRequest;
use App\Models\User;
use App\Models\Workspace;
use App\Notifications\NewUserAddedToWorkspace;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class WorkspacesController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateWorkspaceRequest $request): RedirectResponse
    {
        $workspace = Workspace::create([
            'name' => $request->name,
            'type' => $request->type,
            'description' => $request->description,
            'visibility' => 'private',
        ]);
        $workspace->users()->attach(auth()->id(), ['admin' => 1]);
        session()->flash('success', 'Workspace Created Successfully!');
        return redirect(route('workspaces.show', $workspace));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Workspace  $workspace
     * @return \Illuminate\Http\Response
     */
    public function show(Workspace $workspace): View
    {
        $this->authorize('view', $workspace);
        return view('workspaces.show', compact(['workspace']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Workspace  $workspace
     * @return \Illuminate\Http\Response
     */
    public function edit(Workspace $workspace)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Workspace  $workspace
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateWorkspaceRequest $request, Workspace $workspace): RedirectResponse
    {
        $this->authorize('update', $workspace);
        $workspace->update([
            'name' => $request->name,
            'type' => $request->type,
            'description' => $request->description,
        ]);
        WorkspaceUpdateEvent::dispatch(['auth' => auth()->id(), 'name' => $workspace->name, 'workspace_id' => $workspace->id, 'description' => $workspace->description, 'type' => $workspace->type, 'visibility' => $workspace->visibility]);
        session()->flash('success', 'Workspace Updated Successfully!');
        return redirect(route('workspaces.show', $workspace));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Workspace  $workspace
     * @return \Illuminate\Http\Response
     */
    public function destroy(Workspace $workspace)
    {
        $this->authorize('delete', $workspace);
        $members = $workspace->users;
        foreach($members as $member) {
            WorkspaceEvent::dispatch(['id' => $member->id, 'auth' => auth()->id(), 'name' => $workspace->name, 'workspace_id' => $workspace->id, 'description' => $workspace->description, 'type' => $workspace->type, 'visibility' => $workspace->visibility, 'feature' => 'destroy']);
        }
        $workspace->delete();
        session()->flash('success', 'Workspace Deleted Successfully!');
        return redirect(route('home'));
    }

    public function members(Workspace $workspace): View
    {
        $this->authorize('view', $workspace);
        $users = $workspace->users;
        return view('workspaces._members', compact(['workspace', 'users']));
    }

    public function boards(Workspace $workspace): View
    {
        $this->authorize('view', $workspace);
        $boards = $workspace->boards;
        return view('workspaces._boards', compact(['workspace', 'boards']));
    }

    public function changeVisibility(Workspace $workspace, Request $request)
    {
        $workspace->update([
            'visibility' => $request->visibility,
        ]);
        WorkspaceUpdateEvent::dispatch(['auth' => auth()->id(), 'name' => $workspace->name, 'workspace_id' => $workspace->id, 'description' => $workspace->description, 'type' => $workspace->type, 'visibility' => $workspace->visibility]);
        return response()->json(['visibility' => $request->visibility], 200);
    }

    public function inviteMembers(Workspace $workspace, Request $request): RedirectResponse
    {
        $members = $request->validate([
            'members' => 'required|array',
        ]);
        foreach($members as $member) {
            $workspace->users()->attach($member, ['admin' => 0]);
            WorkspaceEvent::dispatch(['id' => $member[0], 'auth' => auth()->id(), 'name' => $workspace->name, 'workspace_id' => $workspace->id, 'description' => $workspace->description, 'type' => $workspace->type, 'visibility' => $workspace->visibility, 'feature' => 'add']);
            $user = User::find($member[0]);
            $user->notify(new NewUserAddedToWorkspace($workspace));
        }
        // event(new WorkspaceEvent(['id' => $member[0], 'auth' => auth()->id(), 'name' => $workspace->name, 'workspace_id' => $workspace->id, 'description' => $workspace->description, 'type' => $workspace->type, 'visibility' => $workspace->visibility, 'feature' => 'add']));
        session()->flash('success', count($request->members) > 1 ? 'Members Added To Workspace Successfully!' : 'Member Added To Workspace Successfully!');
        return redirect()->back();
    }

    public function removeMember(Workspace $workspace, Request $request)
    {
        $this->authorize('removeMember', $workspace);
        $workspace->users()->detach($request->user_id);
        WorkspaceEvent::dispatch(['id' => $request->user_id, 'auth' => auth()->id(), 'name' => $workspace->name, 'workspace_id' => $workspace->id, 'description' => $workspace->description, 'type' => $workspace->type, 'visibility' => $workspace->visibility, 'feature' => 'remove']);
        return response()->json(['result' => $request->user_id], 200);
    }

    public function leaveWorkspace(Workspace $workspace)
    {
        $workspace->users()->detach(auth()->id());
        session()->flash('success', 'Workspace Left Successfully!');
        return redirect(route('home'));
    }

    public function changeAdminStatus(Workspace $workspace, Request $request)
    {
        $this->authorize('changeAdminStatus', $workspace);
        if(auth()->id() == $request->user_id) {
            return response()->json(['status' => 401], 401);
        }
        $workspace->users()->updateExistingPivot($request->user_id, ['admin' => $request->adminStatus]);
        return response()->json(['result' => $request->adminStatus], 200);
    }
}
