<?php

namespace App\Http\Controllers;

use App\Models\Board;
use App\Models\Card;
use App\Models\User;
use App\Models\Workspace;
use Illuminate\Http\Request;

class AutocompleteController extends Controller
{
    public function autocompleteWorkspace(Request $request)
    {
        $users = [];
        if($request->has('q')) {
            $search = $request->q;
            $workspace_users = [];
            if($request->has('workspace_id')) {
                $workspace_users = Workspace::find($request->workspace_id)->users->pluck('id');
            }
            $users = User::select("id", "email")->where('email', 'LIKE', "%$search%")->whereNotIn('id', $workspace_users)->get();
        }
        return response()->json($users);
    }

    public function autocompleteBoard(Request $request)
    {
        $users = [];
        if($request->has('q')) {
            $search = $request->q;
            $board_users = [];
            if($request->has('board_id')) {
                $board_users = Board::find($request->board_id)->users->pluck('id');
            }
            $users = User::select("id", "email")->where('email', 'LIKE', "%$search%")->whereNotIn('id', $board_users)->get();
        }
        return response()->json($users);
    }

    public function autocompleteCard(Request $request)
    {
        $users = [];
        if($request->has('q')) {
            $search = $request->q;
            $card_users = [];
            if($request->has('card_id')) {
                $card_users = Card::find($request->card_id)->users->pluck('id');
            }
            $users = User::select("id", "email")->where('email', 'LIKE', "%$search%")->whereNotIn('id', $card_users)->get();
        }
        return response()->json($users);
    }
}
