<?php

namespace App\Http\Controllers;

use App\Events\ListEvent;
use App\Events\ListUpdateEvent;
use App\Models\Board;
use App\Models\Lists;
use Illuminate\Http\Request;

class ListsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $board = Board::findOrFail($request->board_id);
        $this->authorize('update', $board);
        $rules = [
            'name' => 'required|max:255',
        ];
        $this->validate($request, $rules);
        $list = $board->lists()->create([
            'name' => $request->name,
        ]);
        ListEvent::dispatch(['id' => $board->id, 'name' => $request->name, 'list_id' => $list->id, 'auth' => auth()->id()]);
        return response()->json(['list' => $request->name, 'list_id' => $list->id], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Lists  $lists
     * @return \Illuminate\Http\Response
     */
    public function show(Lists $lists)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Lists  $lists
     * @return \Illuminate\Http\Response
     */
    public function edit(Lists $lists)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Lists  $lists
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Lists $lists)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Lists  $lists
     * @return \Illuminate\Http\Response
     */
    public function destroy(Lists $lists)
    {
        //
    }

    public function changeName(Request $request)
    {
        $list = Lists::findOrFail($request->list_id);
        $board = Board::findOrFail($request->board_id);
        $this->authorize('update', $board);
        $rules = [
            'newName' => 'required|max:255',
        ];
        $this->validate($request, $rules);
        if($request->newName == $list->name) {
            return response()->json(['result' => $request->list_id], 200);
        }
        $list->update([
            'name' => $request->newName,
        ]);
        ListUpdateEvent::dispatch(['id' => $board->id, 'name' => $request->newName, 'list_id' => $list->id, 'auth' => auth()->id()]);
        return response()->json(['result' => $request->list_id], 200);
    }
}
