<?php

namespace App\Http\Controllers;

use Illuminate\View\View;

class HomeController extends BaseController
{
    public function index(): View
    {
        $recents = auth()->user()->boards()->latest('viewed_at')->limit(5)->get();
        $favourites = auth()->user()->boards()->where('favourite', 1)->get();
        return view('home', compact(['favourites', 'recents']));
    }
}
