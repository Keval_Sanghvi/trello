<?php

namespace App\Http\Controllers;

use App\Events\BoardEvent;
use App\Events\BoardUpdateEvent;
use App\Http\Requests\Boards\CreateBoardRequest;
use App\Models\Board;
use App\Models\User;
use App\Models\Workspace;
use App\Notifications\NewUserAddedToBoard;
use Carbon\Carbon;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class BoardsController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(): View
    {
        $favourites = auth()->user()->boards()->where('favourite', 1)->get();
        $personalBoards = auth()->user()->boards()->whereDoesntHave('workspace')->get();
        $workspaces_ids = auth()->user()->boards()->where('workspace_id', '!=', null)->pluck('workspace_id', 'boards.id')->toArray();
        foreach($workspaces_ids as $key => $value) {
            if(! auth()->user()->workspaces()->where('workspaces.id', $value)->exists()) {
                $board = Board::find($key);
                $personalBoards->push($board);
            }
        }
        $workspaces = auth()->user()->workspaces()->with('boards')->get();
        return view("boards.index", compact(['personalBoards', 'workspaces', 'favourites']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateBoardRequest $request)
    {
        if($request->has('workspace_id')) {
            $this->authorize('addBoard', Workspace::findOrFail($request->workspace_id));
        }
        $data = [
            'title' => $request->title,
            'background' => $request->background,
            'visibility' => $request->visibility,
        ];
        if($request->has('workspace_id')) {
            $data += ['workspace_id' => $request->workspace_id];
        }
        $board = Board::create($data);
        auth()->user()->boards()->attach($board->id, ['admin' => 1, 'viewed_at' => Carbon::now(), 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]);
        session()->flash('success', 'Board Created Successfully!');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Board  $board
     * @return \Illuminate\Http\Response
     */
    public function show(Board $board): View
    {
        $this->authorize('view', $board);
        auth()->user()->boards()->updateExistingPivot($board->id, ['viewed_at' => Carbon::now()]);
        $users = $board->users->take(5);
        $totalUsers = $board->users->count();
        $totalUsers -= $users->count();
        $lists = $board->lists()->with('cards')->get();
        $label_colors = ['#007bff', '#6c757d', '#28a745', '#ffc107', '#dc3545', '#343a40'];
        return view('boards.show', compact(['board', 'users', 'totalUsers', 'lists', 'label_colors']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Board  $board
     * @return \Illuminate\Http\Response
     */
    public function edit(Board $board)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Board  $board
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Board $board)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Board  $board
     * @return \Illuminate\Http\Response
     */
    public function destroy(Board $board)
    {
        //
    }

    public function changeFavouriteStatus(Request $request)
    {
        $favourite = preg_replace('/\s+/', '', $request->mark) == "n" ? 1 : 0;
        auth()->user()->boards()->updateExistingPivot($request->board_id, ['favourite' => $favourite]);
        return response()->json(['result' => $request->mark], 200);
    }

    public function changeTitle(Request $request)
    {
        $board = Board::findOrFail($request->board_id);
        $this->authorize('update', $board);
        $rules = [
            'newTitle' => 'required|max:255',
        ];
        $this->validate($request, $rules);
        if($request->newTitle == $board->title) {
            return response()->json(['result' => $request->board_id], 200);
        }
        $board->update([
            'title' => $request->newTitle,
        ]);
        BoardUpdateEvent::dispatch(['id' => $board->id, 'title' => $board->title, 'background' => $board->background, 'auth' => auth()->id()]);
        return response()->json(['result' => $request->board_id], 200);
    }

    public function changeBackground(Request $request)
    {
        $board = Board::findOrFail($request->board_id);
        $this->authorize('update', $board);
        $rules = [
            'newBackground' => 'required|in:#0079bf,#d29034,#519839,#b04632,#89609e,#cd5a91,#4bbf6b,#00aecc,#838c91',
        ];
        $this->validate($request, $rules);
        if($request->newBackground == $board->background) {
            return response()->json(['result' => $request->board_id, 'background' => $request->newBackground], 200);
        }
        $board->update([
            'background' => $request->newBackground,
        ]);
        BoardUpdateEvent::dispatch(['id' => $board->id, 'title' => $board->title, 'background' => $board->background, 'auth' => auth()->id()]);
        return response()->json(['result' => $request->board_id, 'background' => $request->newBackground], 200);
    }

    public function inviteMembers(Board $board, Request $request): RedirectResponse
    {
        $this->authorize('invite', $board);
        $users = $request->validate([
            'users' => 'required|array',
        ]);
        foreach($users as $user) {
            $board->users()->attach($user, ['admin' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]);
            BoardEvent::dispatch(['id' => $user[0], 'auth' => auth()->id(), 'board_id' => $board->id, 'title' => $board->title, 'background' => $board->background, 'workspace_id' => $board->workspace_id, 'feature' => 'add']);
            $user = User::find($user[0]);
            $user->notify(new NewUserAddedToBoard($board));
        }
        return redirect()->back();
    }
}
