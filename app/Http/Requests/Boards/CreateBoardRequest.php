<?php

namespace App\Http\Requests\Boards;

use Illuminate\Foundation\Http\FormRequest;

class CreateBoardRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:255',
            'background' => 'required|in:#0079bf,#d29034,#519839,#b04632,#89609e,#cd5a91,#4bbf6b,#00aecc,#838c91',
            'visibility' => 'in:private,public,workspace',
            'workspace_id' => 'exists:workspaces,id'
        ];
    }
}
