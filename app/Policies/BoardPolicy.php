<?php

namespace App\Policies;

use App\Models\Board;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class BoardPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Board  $board
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function view(User $user, Board $board)
    {
        if($user->boards()->where('board_id', $board->id)->exists()) {
            return true;
        } else if($board->visibility == "public") {
            return true;
        } else if($board->visibility == 'workspace') {
            return $user->workspaces()->where('workspace_id', $board->workspace_id)->exists();
        }
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Board  $board
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function update(User $user, Board $board)
    {
        if($user->boards()->where('board_id', $board->id)->exists()) {
            return true;
        } else if($board->visibility == 'workspace') {
            return $user->workspaces()->where('workspace_id', $board->workspace_id)->exists();
        }
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Board  $board
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function delete(User $user, Board $board)
    {
        //
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Board  $board
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function restore(User $user, Board $board)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Board  $board
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function forceDelete(User $user, Board $board)
    {
        //
    }

    public function hasBoard(User $user, Board $board)
    {
        return $user->boards()->where('boards.id', $board->id)->exists();
    }

    public function invite(User $user, Board $board)
    {
        if($user->boards()->where('board_id', $board->id)->exists()) {
            return true;
        } else if($board->visibility == 'workspace') {
            return $user->workspaces()->where('workspace_id', $board->workspace_id)->exists();
        }
    }
}
