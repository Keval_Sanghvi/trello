<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Workspace;
use Illuminate\Auth\Access\HandlesAuthorization;

class WorkspacePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Workspace  $workspace
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function view(User $user, Workspace $workspace)
    {
        return $user->workspaces()->where('workspace_id', $workspace->id)->exists() || $workspace->visibility == 'public';
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Workspace  $workspace
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function update(User $user, Workspace $workspace)
    {
        return $user->workspaces()->where(['workspace_id' => $workspace->id, 'admin' => 1])->exists();
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Workspace  $workspace
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function delete(User $user, Workspace $workspace)
    {
        return $user->workspaces()->where(['workspace_id' => $workspace->id, 'admin' => 1])->exists();
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Workspace  $workspace
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function restore(User $user, Workspace $workspace)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Workspace  $workspace
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function forceDelete(User $user, Workspace $workspace)
    {
        //
    }

    public function changeVisibility(User $user, Workspace $workspace)
    {
        return $user->workspaces()->where(['workspace_id' => $workspace->id, 'admin' => 1])->exists();
    }

    public function inviteMembers(User $user, Workspace $workspace)
    {
        return $user->workspaces()->where(['workspace_id' => $workspace->id, 'admin' => 1])->exists();
    }

    public function removeMember(User $user, Workspace $workspace)
    {
        return $user->workspaces()->where(['workspace_id' => $workspace->id, 'admin' => 1])->exists();
    }

    public function changeAdminStatus(User $user, Workspace $workspace)
    {
        return $user->workspaces()->where(['workspace_id' => $workspace->id, 'admin' => 1])->exists();
    }

    public function addBoard(User $user, Workspace $workspace)
    {
        return $user->workspaces()->where(['workspace_id' => $workspace->id])->exists();
    }
}
