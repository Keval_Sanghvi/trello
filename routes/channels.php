<?php

use App\Models\Workspace;
use Illuminate\Support\Facades\Broadcast;

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('App.Models.User.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});

Broadcast::channel('workspace.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});

Broadcast::channel('workspace-update.{id}', function ($user, $id) {
    $workspace = Workspace::findOrFail($id);
    return $workspace->users->contains($user->id);
});

Broadcast::channel('board.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});

Broadcast::channel('board-update.{id}', function ($user, $id) {
    return $user->boards->contains($id);
});

Broadcast::channel('list.{id}', function ($user, $id) {
    return $user->boards->contains($id);
});

Broadcast::channel('list-update.{id}', function ($user, $id) {
    return $user->boards->contains($id);
});

Broadcast::channel('card.{id}', function ($user, $id) {
    return $user->boards->contains($id);
});

Broadcast::channel('card-update.{id}', function ($user, $id) {
    return $user->boards->contains($id);
});
