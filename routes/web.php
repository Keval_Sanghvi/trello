<?php

use App\Http\Controllers\AutocompleteController;
use App\Http\Controllers\BoardsController;
use App\Http\Controllers\CardsController;
use App\Http\Controllers\ChecklistsController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ListsController;
use App\Http\Controllers\WorkspacesController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['auth'])->group(function() {
    Route::get('/home', [HomeController::class, 'index'])->name('home');
    Route::resource('workspaces', WorkspacesController::class)->except('create', 'edit');
    Route::get('workspaces/{workspace}/members', [WorkspacesController::class, 'members'])->name('workspaces.members');
    Route::get('workspaces/{workspace}/boards', [WorkspacesController::class, 'boards'])->name('workspaces.boards');
    Route::post('workspaces/{workspace}/change-visibility', [WorkspacesController::class, 'changeVisibility'])->name('workspaces.changeVisibility');

    Route::get('autocomplete-search-workspace', [AutocompleteController::class, 'autocompleteWorkspace']);
    Route::get('autocomplete-search-board', [AutocompleteController::class, 'autocompleteBoard']);
    Route::get('autocomplete-search-card', [AutocompleteController::class, 'autocompleteCard']);
    Route::post('workspaces/{workspace}/invite-members', [WorkspacesController::class, 'inviteMembers'])->name('workspaces.inviteMembers');

    Route::post('workspaces/{workspace}/remove-member', [WorkspacesController::class, 'removeMember'])->name('workspaces.removeMember');
    Route::post('workspaces/{workspace}/leave-workspace', [WorkspacesController::class, 'leaveWorkspace'])->name('workspaces.leaveWorkspace');
    Route::post('workspaces/{workspace}/change-admin-status', [WorkspacesController::class, 'changeAdminStatus'])->name('workspaces.changeAdminStatus');

    Route::resource('boards', BoardsController::class)->except('create', 'edit', 'update', 'destroy');
    Route::post('boards/change-favourite-status', [BoardsController::class, 'changeFavouriteStatus'])->name('boards.changeFavouriteStatus');
    Route::post('boards/change-title', [BoardsController::class, 'changeTitle'])->name('boards.changeTitle');
    Route::post('boards/change-background', [BoardsController::class, 'changeBackground'])->name('boards.changeBackground');
    Route::post('boards/{board}/invite-members', [BoardsController::class, 'inviteMembers'])->name('boards.inviteMembers');
    Route::post('boards/lists/create', [ListsController::class, 'store'])->name('lists.store');
    Route::post('boards/lists/cards/create', [CardsController::class, 'store'])->name('cards.store');
    Route::post('boards/lists/cards/changeList', [CardsController::class, 'changeList'])->name('cards.changeList');
    Route::post('lists/change-name', [ListsController::class, 'changeName'])->name('lists.changeName');

    Route::post('cards/getCard', [CardsController::class, 'getCard'])->name('cards.getCard');
    Route::post('cards/change-name', [CardsController::class, 'changeName'])->name('cards.changeName');
    Route::post('cards/change-description', [CardsController::class, 'changeDescription'])->name('cards.changeDescription');
    Route::post('cards/change-duedate', [CardsController::class, 'changeDueDate'])->name('cards.changeDueDate');
    Route::post('cards/change-status', [CardsController::class, 'changeStatus'])->name('cards.changeStatus');
    Route::post('boards/{board}/cards/{card}/invite-members', [CardsController::class, 'inviteMembers'])->name('cards.inviteMembers');
    Route::post('cards/checklists/create', [ChecklistsController::class, 'store'])->name('checklists.store');
    Route::post('cards/checklists/change-status', [ChecklistsController::class, 'changeStatus'])->name('checklists.changeStatus');
    Route::post('cards/checklists/remove', [ChecklistsController::class, 'remove'])->name('checklists.remove');

    Route::post('cards/labels/create', [CardsController::class, 'createLabel'])->name('labels.store');
    Route::post('cards/labels/destroy', [CardsController::class, 'removeLabel'])->name('labels.destroy');
});

require __DIR__.'/auth.php';
