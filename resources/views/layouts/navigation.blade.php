<nav class="navbar navbar-expand-lg background-primary d-flex justify-content-between">
    <div class="navbar-nav col-4 align-items-start ">
        <a href="{{ route('home') }}" class="btn btn-color deco-none">
            <i class="fa fa-home text-light" aria-hidden="true"></i>
        </a>
        <a href="{{ route("boards.index") }}" class="btn btn-color deco-none text-light ml-2">
            <i class="fa fa-trello text-light" aria-hidden="true"></i>
            Boards
        </a>
    </div>

    <div class="col-4 d-flex justify-content-center">
        <a href="{{ route('home') }}" class="deco-none my-font d-flex align-items-baseline mt-2">
            <i class="fa fa-trello mr-2 text-light fa-lg" aria-hidden="true"></i>
            <span class="h5 text-light font-weight-bold">Trello</span>
        </a>
    </div>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="col-4 pr-0 d-flex justify-content-end">
        <div class="collapse navbar-collapse mr-2" id="navbarSupportedContent1">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown">
                    @auth
                        <a class="nav-link btn btn-color deco-none text-light" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Create
                        </a>
                        <div class="dropdown-menu" style="width: 260px !important; left: -190px" aria-labelledby="navbarDropdown">
                            <div class="text-center pb-2 border-bottom">Create</div>
                            <div class="p-2 dropdown-item">
                                <a href="" class="deco-none d-flex flex-column" data-toggle="modal" data-target=".create-board-modal">
                                    <div>
                                        <i class="fa fa-trello mr-2" aria-hidden="true"></i>
                                        Create Board
                                    </div>
                                    <p class="mt-2 w-100" style="white-space: initial; font-size: 13px;">A board is made up of cards ordered on lists. Use it to manage projects, track information, or organize anything.</p>
                                </a>
                            </div>
                            <hr class="m-0">
                            <div class="p-2 dropdown-item">
                                <a href="" class="deco-none d-flex flex-column" data-toggle="modal" data-target=".create-workspace-modal">
                                    <div>
                                        <i class="fa fa-users mr-2" aria-hidden="true"></i>
                                        Create Workspace
                                    </div>
                                    <p class="mt-2 w-100" style="white-space: initial; font-size: 13px;">A Workspace is a group of boards and people. Use it to organize your company, side hustle, family, or friends.</p>
                                </a>
                            </div>
                        </div>
                    @endauth
                </li>
            </ul>
        </div>

        <a href="{{ route('home') }}" class="btn btn-color deco-none mr-2">
            <i class="fa fa-info-circle text-light" aria-hidden="true"></i>
        </a>

        <div class="d-flex justify-content-end">
            <div class="collapse navbar-collapse mr-2" id="navbarSupportedContent2">
                <ul class="navbar-nav">
                    <li class="nav-item dropdown">
                        @auth
                            <a class="nav-link btn btn-color deco-none text-light" style="padding: 8px 12px !important;" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-bell-o text-light" aria-hidden="true"></i>
                            </a>
                            <div class="dropdown-menu" style="width: 420px !important; left: -380px" aria-labelledby="navbarDropdown">
                                <div class="text-center pb-2 border-bottom">Notifications</div>
                                @if(auth()->user()->notifications()->count() == 0)
                                    <div class="mt-5 mb-5 d-flex align-items-center flex-column">
                                        <img src="{{ asset('images/notification.svg') }}" alt="No Notifications" class="img-fluid">
                                        <h3 class="mt-4">No Notifications</h3>
                                    </div>
                                @else
                                    <div class="p-2" style="overflow-y: auto; max-height: 400px;">
                                        @foreach($notifications as $notification)
                                            <div class="notification mt-3 mb-3">
                                                @if($notification->type === \App\Notifications\NewUserAddedToWorkspace::class)
                                                    You were added to new Workspace - <strong>{{ $notification->data['workspace']['name'] }}</strong>
                                                    <a href="{{ route('workspaces.show', $notification->data['workspace']['id']) }}" style="width: 35%;" class="btn mt-2 d-block background-primary btn-sm text-light deco-none">View Workspace</a>
                                                @elseif($notification->type === \App\Notifications\NewUserAddedToBoard::class)
                                                    You were added to new Board - <strong>{{ $notification->data['board']['title'] }}</strong>
                                                    <a href="{{ route('boards.show', $notification->data['board']['id']) }}" style="width: 35%;" class="btn mt-2 d-block background-primary btn-sm text-light deco-none">View Board</a>
                                                @elseif($notification->type === \App\Notifications\NewUserAddedToCard::class)
                                                    You were added to new Card - <strong>{{ $notification->data['card']['name'] }}</strong>
                                                    <a href="{{ route('boards.show', $notification->data['board']['id']) }}" style="width: 35%;" class="btn mt-2 d-block background-primary btn-sm text-light deco-none">View Card</a>
                                                @elseif($notification->type === \App\Notifications\CardDueDate::class)
                                                    Due Date is close of card - <strong>{{ $notification->data['card']['name'] }}</strong>
                                                    <a href="{{ route('boards.show', $notification->data['board']['id']) }}" style="width: 35%;" class="btn mt-2 d-block background-primary btn-sm text-light deco-none">View Card</a>
                                                @endif
                                            </div>
                                            <hr>
                                        @endforeach
                                    </div>
                                @endif
                            </div>
                        @endauth
                    </li>
                </ul>
            </div>
        </div>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown">
                    @auth
                        <a class="p-0 nav-link my-font" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img src="{{ auth()->user()->avatar }}" alt="Avatar">
                        </a>
                        <div class="dropdown-menu" style="width: 260px !important; left: -205px" aria-labelledby="navbarDropdown">
                            <div class="text-center pb-2 border-bottom">Account</div>
                            <div class="d-flex pb-3 pt-3 pl-1 pr-1 border-bottom">
                                <img src="{{ auth()->user()->avatar }}" alt="Avatar" class="mr-3">
                                <div>
                                    <strong class="d-block">{{ auth()->user()->name }}</strong>
                                    <strong class="small font-weight-normal">{{ auth()->user()->email }}</strong>
                                </div>
                            </div>
                            <div class="border-bottom dropdown-item">
                                <a href="{{ route('home') }}" class="d-inline-block w-100 p-2" style="color: inherit; text-decoration:none;">Home</a>
                            </div>
                            <div class="border-bottom dropdown-item">
                                <a href="" class="d-inline-block w-100 p-2" style="color: inherit; text-decoration:none;">Cards</a>
                            </div>
                            <form action="{{ route('logout') }}" method="POST">
                                @csrf
                                <input type="submit" class="dropdown-item pt-2 pb-2" style="cursor: pointer; padding-left: 33px;" value="Logout">
                            </form>
                        </div>
                    @else
                        <a href="{{ route('login') }}" class="btn btn-sm btn-primary mr-3">Log in</a>
                        @if (Route::has('register'))
                            <a href="{{ route('register') }}" class="btn btn-sm btn-primary mr-3">Register</a>
                        @endif
                    @endauth
                </li>
            </ul>
        </div>
    </div>
</nav>
