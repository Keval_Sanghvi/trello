<div class="col-md-3">
    <div class="list-group">
        <a class="list-group-item @if(\Request::route()->getName() == 'home')active @endif" href="{{ route('home') }}">
            <i class="fa fa-home mr-2" aria-hidden="true"></i>
            Home
        </a>
        <a class="list-group-item @if(\Request::route()->getName() == 'boards.index')active @endif" href="{{ route("boards.index") }}">
            <i class="fa fa-trello mr-2" aria-hidden="true"></i>
            Boards
        </a>
    </div>
    <div class="d-flex justify-content-between pt-4">
        <p class="text-uppercase text-muted m-0 p-2">Workspaces</p>
        <a href="" data-toggle="modal" data-target=".create-workspace-modal" class="p-2">
            <i class="fa fa-plus text-muted" aria-hidden="true"></i>
        </a>
    </div>
    <div class="pl-2 pr-2 pt-2" id="workspaces">
        @foreach($workspaces as $workspace)
            <div>
                <a class="deco-none d-block mt-3" id="workspace-{{ $workspace->id }}" data-toggle="collapse" href="#collapse-{{ $workspace->id }}" role="button" aria-expanded="false" aria-controls="collapseExample" style="font-size: 15px;">
                    <span style="background: linear-gradient(#cc4223, #cb7d25); padding: .3rem .7rem;" class="rounded text-white mr-2">{{ $workspace->name[0] }}</span>
                    {{ $workspace->name }}
                    <i class="fa fa-caret-down ml-2" aria-hidden="true"></i>
                </a>
                <div class="collapse mt-2" id="collapse-{{ $workspace->id }}">
                    <div class="list-group">
                        <a href="{{ route('workspaces.show', $workspace->id) }}" class="list-group-item list-group-item-action">Workspace</a>
                        <a href="{{ route('workspaces.boards', $workspace->id) }}" class="list-group-item list-group-item-action">Boards</a>
                        <a href="{{ route('workspaces.members', $workspace->id) }}" class="list-group-item list-group-item-action">Members</a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
