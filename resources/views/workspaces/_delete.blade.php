<!-- Modal -->
<div class="modal fade" id="delete-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Delete Workspace</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('workspaces.destroy', $workspace->id) }}" method="POST" id="deleteWorkspaceForm">
                @csrf
                @method("DELETE")
                <div class="modal-body">
                    Deleting a Workspace is permanent. Are you sure you want to delete this Workspace? There is no undo. Boards with this Workspace won't be deleted. Your boards in this Workspace will appear in your personal boards list.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-outline-danger">Delete</button>
                </div>
            </form>
        </div>
    </div>
</div>
