@extends('workspaces.show')

@section('workspace-content')
    @can('inviteMembers', $workspace)
        <div class="card mb-4">
            <div class="card-header">
                <h2>Invite Your Team</h2>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-8">
                        <p class="mb-4">Trello makes teamwork your best work. Invite your new team members to get going!</p>
                        <form action="{{ route('workspaces.inviteMembers', $workspace->id) }}" method="POST" id="add-members-form">
                            @csrf
                            <div class="form-group @error('members') is-invalid @enderror">
                                <label for="members" class="font-weight-bold">Workspace members</label>
                                <select name="members[]" id="members" class="form-control select2" multiple="multiple" data-error=".members_error"></select>
                                <small class="members_error text-danger h6"></small>
                                @error('members')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                @enderror
                                <small class="mt-2 d-block"><strong>Pro tip! Paste as many emails here as needed.</strong></small>
                            </div>
                            <button type="submit" class="btn btn-sm btn-success">Invite to Workspace</button>
                        </form>
                    </div>
                    <div class="col-4">
                        <img src="{{ asset('images/invite.svg') }}" class="w-100 img-fluid" alt="Invite Members">
                    </div>
                </div>
            </div>
        </div>
    @endcan
    <div class="card">
        <div class="card-header">
            <h2>Members</h2>
        </div>
        <div class="card-body">
            <table class="table" id="membersTable">
                <thead>
                    <th>Avatar</th>
                    <th>Name</th>
                    <th>Actions</th>
                </thead>
                <tbody>
                    @foreach($users as $user)
                    <tr id="user-{{ $user->id }}">
                        <td><img src="{{ $user->avatar }}" alt="Avatar"></td>
                        <td>{{ $user->name }}</td>
                        <td>
                            <button data-id="{{ $user->id }}" class="btn btn-sm btn-outline-secondary changeAdminStatus @cannot('changeAdminStatus', $workspace) disabled @endcannot @if(auth()->id() == $user->id) disabled @endif" @cannot('changeAdminStatus', $workspace) disabled @endcannot @if(auth()->id() == $user->id) disabled @endif>
                                @if($user->getAdminStatusForWorkspace($workspace) == 'admin')
                                    <i class="fa fa-smile-o mr-2" aria-hidden="true"></i>
                                    Admin
                                @else
                                    <i class="fa fa-frown-o mr-2" aria-hidden="true"></i>
                                    Normal
                                @endif
                            </button>
                            @if(auth()->id() === $user->id)
                                <form action="{{ route('workspaces.leaveWorkspace', $workspace->id) }}" method="POST" class="d-inline-block">
                                    @csrf
                                    <button class="btn btn-sm btn-outline-secondary">
                                        <i class="fa fa-times mr-2" aria-hidden="true"></i>
                                        Leave
                                    </button>
                                </form>
                            @else
                                @can('removeMember', $workspace)
                                    <button class="btn btn-sm btn-outline-secondary removeMember" data-id="{{ $user->id }}">
                                        <i class="fa fa-times mr-2" aria-hidden="true"></i>
                                        Remove
                                    </button>
                                @endcan
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
