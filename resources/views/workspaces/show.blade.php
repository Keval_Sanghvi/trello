@extends('layouts.app')

@section('page-level-styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha512-SfTiTlX6kk+qitfevl/7LibUOeJWlt9rbyDn92a1DqWOw9vWG2MFoays0sgObmWazO5BQPiFucnnEAjpAB+/Sw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endsection

@section('main-content')
    <div class="container">
        <div class="row mt-4 mb-4">
           <div class="col-md-12" id="message">
                @include('layouts.partials._message')
           </div>
        </div>
        <div class="row mt-5 mb-5">
            <div class="col-md-3">
                <div class="list-group">
                    <a class="list-group-item @if(\Request::route()->getName() == 'workspaces.show')active @endif" href="{{ route('workspaces.show', $workspace->id) }}">
                        <i class="fa fa-home mr-2" aria-hidden="true"></i>
                        Workspace
                    </a>
                    <a class="list-group-item @if(\Request::route()->getName() == 'workspaces.boards')active @endif" href="{{ route('workspaces.boards', $workspace->id) }}">
                        <i class="fa fa-trello mr-2" aria-hidden="true"></i>
                        Boards
                    </a>
                    <a class="list-group-item @if(\Request::route()->getName() == 'workspaces.members')active @endif" href="{{ route('workspaces.members', $workspace->id) }}">
                        <i class="fa fa-users mr-2" aria-hidden="true"></i>
                        Members
                    </a>
                    <a class="list-group-item" href="{{ route("home") }}">
                        <i class="fa fa-bolt mr-2" aria-hidden="true"></i>
                        Home
                    </a>
                </div>
            </div>
            <div class="col-md-9">
                @if(Route::is('workspaces.show'))
                    <div class="text-center mb-5">
                        <h3 class="mr-4 mb-0 d-inline-block name">{{ $workspace->name }}</h3>
                        <div class="mt-2 d-inline-block" id="workspace-visibility">
                            @if($workspace->visibility == 'private')
                                <i class="fa fa-lock text-danger" aria-hidden="true"></i>
                                <span>Private</span>
                            @else
                                <i class="fa fa-globe text-success" aria-hidden="true"></i>
                                <span>Public</span>
                            @endif
                        </div>
                    </div>
                    @can('update', $workspace)
                        <a href="" data-toggle="modal" data-target=".edit-workspace-modal" class="btn btn-sm btn-light float-right">
                            <i class="fa fa-pencil mr-2" aria-hidden="true"></i>
                            Edit Your Workspace
                        </a>
                    @endcan
                    <div class="desc">
                        @if($workspace->description)
                            <p class="ml-5 mt-4 description"><span class="font-weight-bold">Description:</span> {{ $workspace->description }}</p>
                        @endif
                    </div>
                    <p class="ml-5 mt-4 type"><span class="font-weight-bold">Workspace Type:</span> {{ $workspace->type }}</p>
                    <div>
                        <p class="ml-5 mt-4 pb-3 border-bottom"><span class="font-weight-bold">Workspace Visibility: </span></p>
                        <div class="ml-5 d-inline-block d-flex align-items-start">
                            <div id="visibility">
                                @if($workspace->visibility == 'private')
                                    <i class="fa fa-lock text-danger" aria-hidden="true"></i>
                                    <span><strong>Private</strong> - This Workspace is private. It's not indexed or visible to those outside the Workspace.</span>
                                @else
                                    <i class="fa fa-globe text-success" aria-hidden="true"></i>
                                    <span><strong>Public</strong> - This Workspace is public. It's visible to anyone with the link and will show up in search engines like Google. Only those invited to the Workspace can add and edit Workspace boards.</span>
                                @endif
                            </div>
                            @can('changeVisibility', $workspace)
                                <button type="button" class="btn btn-sm btn-outline-info ml-3" id="change-visibility" data-visibility="{{ $workspace->visibility }}">Change</button>
                            @endcan
                        </div>
                    </div>
                    @can('delete', $workspace)
                        <button type="button" class="btn btn-sm btn-outline-danger ml-5 mt-4" data-toggle="modal" data-target="#delete-modal" id="delete-workspace">Delete this Workspace?</button>
                    @endcan
                @endif

                @yield('workspace-content')

                @include('workspaces._edit')

                @include('workspaces._delete')
            </div>
        </div>
    </div>
@endsection

@section('page-level-scripts')
    @yield('page-level-script')
    <script src="{{ asset('js/workspaces/edit-validation.js') }}"></script>

    <script>
        @error('name')
            $('.edit-workspace-modal').modal('show');
        @enderror
        @error('description')
            $('.edit-workspace-modal').modal('show');
        @enderror
        @error('type')
            $('.edit-workspace-modal').modal('show');
        @enderror
    </script>

    <script>
        $("#change-visibility").click(function(e) {
            let visibility = $(this).data("visibility") == 'private' ? 'public' : 'private';
            $.ajax({
                type: 'POST',
                url: `{{ route('workspaces.changeVisibility', $workspace->id) }}`,
                data: {_token: '{{ csrf_token() }}', 'visibility': visibility },
                success: function(data) {
                    if(data) {
                        let html = data.visibility == 'private' ?
                                    `<i class="fa fa-lock text-danger" aria-hidden="true"></i>
                                    <span><strong>Private</strong> - This Workspace is private. It's not indexed or visible to those outside the Workspace.</span>`
                                    :
                                    `<i class="fa fa-globe text-success" aria-hidden="true"></i>
                                    <span><strong>Public</strong> - This Workspace is public. It's visible to anyone with the link and will show up in search engines like Google. Only those invited to the Workspace can add and edit Workspace boards.</span>`;
                        $("#visibility").html(html);

                        $("#change-visibility").data("visibility", data.visibility);
                        $("#change-visibility").blur();

                        html = data.visibility == 'private' ?
                                    `<i class="fa fa-lock text-danger" aria-hidden="true"></i>
                                    <span>Private</span>`
                                    :
                                    `<i class="fa fa-globe text-success" aria-hidden="true"></i>
                                    <span>Public</span>`;
                        $("#workspace-visibility").html(html);
                    }
                },
                error: function(data) {
                    if(data.status == 401 || data.status == 403) {
                        let url = "{{ route('home') }}";
                        document.location.href = url;
                    }
                }
            });
        });
    </script>

    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>
        $('#members').select2({
            allowClear: true,
            placeholder: 'e.g. calrissian@cloud.ci',
            ajax: {
                url: '/autocomplete-search-workspace',
                dataType: 'json',
                delay: 250,
                data: function(params) {
                    return {
                        q: $.trim(params.term),
                        workspace_id: {{ $workspace->id }}
                    };
                },
                processResults: function(data) {
                    return {
                        results: $.map(data, function(item) {
                            return {
                                text: item.email,
                                id: item.id
                            }
                        })
                    };
                }
            }
        });
    </script>

    <script>
        $("#add-members-form").validate({
            ignore: [],
            rules: {
                'members[]': {
                    required: true
                }
            },
            errorElement: 'small',
            errorPlacement: function(error, element) {
                var placement = $(element).data('error');
                if (placement) {
                    $(placement).append(error)
                } else {
                    error.addClass('text-danger');
                    error.insertAfter(element);
                }
            }
        });
    </script>

    <script>
        $("#membersTable").on('click', '.removeMember', function(e) {
            let id = $(this).data("id");
            $.ajax({
                type: 'POST',
                url: `{{ route('workspaces.removeMember', $workspace->id) }}`,
                data: {_token: '{{ csrf_token() }}', 'user_id': id },
                success: function(data) {
                    if(data) {
                        let user_id = data.result;
                        $(`#membersTable #user-${user_id}`).remove();
                        $("#message").html(`<div class="alert alert-success alert-dismissible fade show" role="alert">
                                                Member Removed From Workspace Successfully!
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>`);
                    }
                },
                error: function(data) {
                    if(data.status == 401 || data.status == 403) {
                        let url = "{{ route('home') }}";
                        document.location.href = url;
                    }
                }
            });
        });
    </script>

    <script>
         $("#membersTable").on('click', '.changeAdminStatus', function(e) {
            let id = $(this).data("id");
            $.ajax({
                type: 'POST',
                url: `{{ route('workspaces.changeAdminStatus', $workspace->id) }}`,
                data: {_token: '{{ csrf_token() }}', 'user_id': id, 'adminStatus': $(this).text().trim() == "Admin" ? 0 : 1},
                success: function(data) {
                    if(data) {
                        let adminStatus = data.result;
                        if(adminStatus == 1) {
                            $(`#membersTable #user-${id} .changeAdminStatus`).html(`
                                <i class="fa fa-smile-o mr-2" aria-hidden="true"></i>
                                Admin
                            `);
                        } else {
                            $(`#membersTable #user-${id} .changeAdminStatus`).html(`
                                <i class="fa fa-frown-o mr-2" aria-hidden="true"></i>
                                Normal
                            `);
                        }
                        $("#message").html(`<div class="alert alert-success alert-dismissible fade show" role="alert">
                                                Admin Status Changed Successfully!
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>`);
                    }
                },
                error: function(data) {
                    if(data.status == 401 || data.status == 403) {
                        let url = "{{ route('home') }}";
                        document.location.href = url;
                    }
                }
            });
        });
    </script>

    <script>
        var auth = {{ auth()->id() }};
        var workspace_id = {{ $workspace->id }};

        Echo.private(`workspace.${auth}`)
            .listen('WorkspaceEvent', (e) => {
                let workspace_id = e.workspace.workspace_id;
                let workspace_name = e.workspace.name;
                if((e.workspace.feature == "destroy" && auth != e.workspace.auth) || (e.workspace.feature == "remove" && e.workspace.visibility == "private" && auth != e.workspace.auth)) {
                    let url = "{{ route('home') }}";
                    document.location.href = url;
                } else if(e.workspace.feature == "remove" && e.workspace.visibility == "public" && auth != e.workspace.auth) {
                    $("#create-board").remove();
                }
            });

        Echo.private(`workspace-update.${workspace_id}`)
            .listen('WorkspaceUpdateEvent', (e) => {
                if(auth != e.workspace.auth) {
                    $(".desc").html(`
                        <p class="ml-5 mt-4 description"><span class="font-weight-bold">Description:</span> ${e.workspace.description}</p>
                    `);
                    if(e.workspace.description == null) {
                        $(".description").remove();
                    }
                    $(".type").html(`
                        <span class="font-weight-bold">Workspace Type:</span> ${e.workspace.type}
                    `);
                    $(".name").text(e.workspace.name);
                    if(e.workspace.visibility == "private") {
                        $("#visibility").html(`
                            <i class="fa fa-lock text-danger" aria-hidden="true"></i>
                            <span><strong>Private</strong> - This Workspace is private. It's not indexed or visible to those outside the Workspace.</span>
                        `);
                        $("#workspace-visibility").html(`
                            <i class="fa fa-lock text-danger" aria-hidden="true"></i>
                            <span>Private</span>
                        `);
                    } else {
                        $("#visibility").html(`
                            <i class="fa fa-globe text-success" aria-hidden="true"></i>
                            <span><strong>Public</strong> - This Workspace is public. It's visible to anyone with the link and will show up in search engines like Google. Only those invited to the Workspace can add and edit Workspace boards.</span>
                        `);
                        $("#workspace-visibility").html(`
                            <i class="fa fa-globe text-success" aria-hidden="true"></i>
                            <span>Public</span>
                        `);
                    }
                }
            });
    </script>
@endsection
