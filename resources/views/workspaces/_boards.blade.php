@extends('workspaces.show')

@section('workspace-content')
    <div>
        <div class="favourites">
            @if(! $boards->isEmpty())
                <h5 class="mb-3">
                    <i class="fa fa-user mr-2"></i>
                    Your Workspace Boards
                </h5>
                <div class="d-flex justify-content-start align-items-center flex-wrap mt-4 mb-4 favs" style="gap: 10px;">
                    @foreach($boards as $board)
                        <a href="{{ route('boards.show', $board->id) }}" style="background: {{ $board->background }}; height: 100px; width: 180px;" class="deco-none rounded board @if(! $board->isFavourite()) fav @endif">
                            <h5 class="ml-2 mt-2 text-white">{{ $board->title }}</h5>
                            @if($board->isFavourite() == 1)
                                <span>
                                    <i class="fa fa-star text-warning float-right star" style="position: relative; top: 38px; left: -10px;" aria-hidden="true" data-star="y" data-id="{{ $board->id }}"></i>
                                </span>
                            @else
                                <span class="d-none">
                                    <i class="fa fa-star-o text-warning float-right star" style="position: relative; top: 38px; left: -10px;" aria-hidden="true" data-star="n" data-id="{{ $board->id }}"></i>
                                </span>
                            @endif
                        </a>
                    @endforeach
                    @can('addBoard', $workspace)
                        <button id="create-board" onClick="selectOption('{{ $workspace->id }}')" style="height: 100px; width: 180px;" class="rounded btn btn-light" data-toggle="modal" data-target=".create-board-modal">
                            <p class="m-0 text-dark">Create new board</p>
                        </button>
                    @endcan
                </div>
            @else
                <div class="row shadow-sm pt-2 pb-2 m-0">
                    <div class="col-md-3">
                        <img src="{{ asset('images/workspace.svg') }}" alt="">
                    </div>
                    <div class="col-md-9">
                        <p class="h6 mt-4">Welcome to the Workspace!</p>
                        <p>This workspace contains no boards!</p>
                    </div>
                </div>
                @can('addBoard', $workspace)
                    <button id="create-board" onClick="selectOption('{{ $workspace->id }}')" style="height: 100px; width: 180px;" class="rounded btn btn-light mt-4" data-toggle="modal" data-target=".create-board-modal">
                        <p class="m-0 text-dark">Create new board</p>
                    </button>
                @endcan
            @endif
        </div>
    </div>
@endsection

@section('page-level-script')
    <script>
        function selectOption(workspace_id) {
            $("#select-workspace").val(workspace_id);
        }
    </script>

    <script>
        $(document).on("mouseenter", ".board", function() {
            $(this).css('cursor', 'pointer');
            if($(this).hasClass('fav')) {
                $(this).children().last().removeClass("d-none");
            }
        });

        $(document).on("mouseleave", ".board", function() {
            $(this).css('cursor', 'default');
            if($(this).hasClass('fav')) {
                $(this).children().last().addClass("d-none");
            }
        });
    </script>

    <script>
        $(".star").click(function(e) {
            e.preventDefault();
            let id = $(this).data("id");
            let mark = $(this).data("star");
            $.ajax({
                type: 'POST',
                url: `{{ route('boards.changeFavouriteStatus') }}`,
                data: {_token: '{{ csrf_token() }}', 'board_id': id, 'mark': mark },
                success: function(data) {
                    if(data) {
                        if(mark.includes("n")) {
                            $(`.board [data-id=${id}]`).attr("data-star", "y");
                            $(`.board [data-id=${id}]`).data("star", "y");
                            $(`.board [data-id=${id}]`).removeClass("fa-star-o");
                            $(`.board [data-id=${id}]`).addClass("fa-star");
                            $(`.board [data-id=${id}]`).parent().removeClass("d-none");
                            $(`.board [data-id=${id}]`).parent().parent().removeClass("fav");
                            $(`.board [data-id=${id}]`).parent().parent().off();
                        } else {
                            $(`.board [data-id=${id}]`).attr("data-star", "n");
                            $(`.board [data-id=${id}]`).data("star", "n");
                            $(`.board [data-id=${id}]`).removeClass("fa-star");
                            $(`.board [data-id=${id}]`).addClass("fa-star-o");
                            $(`.board [data-id=${id}]`).parent().addClass("d-none");
                            $(`.board [data-id=${id}]`).parent().parent().addClass("fav");
                            $(`.board [data-id=${id}]`).parent().parent().bind({
                                mouseenter: function() {
                                    $(this).children().last().removeClass("d-none");
                                    $(this).css('cursor', 'pointer');
                                },
                                mouseleave: function() {
                                    $(this).children().last().addClass("d-none");
                                    $(this).css('cursor', 'default');
                                }
                            });
                        }
                    }
                },
                error: function(data) {
                    if(data.status == 401 || data.status == 403) {
                        let url = "{{ route('home') }}";
                        document.location.href = url;
                    }
                }
            });
        });
    </script>

    <script>
        var auth = {{ auth()->id() }};
        var boards = {{ json_encode(auth()->user()->boards()->where('boards.workspace_id', $workspace->id)->pluck('boards.id')->toArray()) }};
        for(var i = 0; i < boards.length; i++) {
            Echo.private(`board-update.${boards[i]}`)
                .listen('BoardUpdateEvent', (e) => {
                    if(auth != e.board.auth) {
                        $(`.favs [data-id=${e.board.id}]`).parent().siblings("h5").text(e.board.title);
                        $(`.favs [data-id=${e.board.id}]`).parent().parent().css('background-color', e.board.background);
                    }
                });
        }

        Echo.private(`board.${auth}`)
            .listen('BoardEvent', (e) => {
                let workspace_id = e.board.workspace_id;
                if(e.board.feature == "add") {
                    if(workspace_id == {{ $workspace->id }}) {
                        $("#create-board").before(`
                            <a href="/boards/${e.board.board_id}" style="background: ${e.board.background}; height: 100px; width: 180px;" class="deco-none rounded board fav">
                                <h5 class="ml-2 mt-2 text-white">${e.board.title}</h5>
                                <span class="d-none">
                                    <i class="fa fa-star-o text-warning float-right star" style="position: relative; top: 38px; left: -10px;" aria-hidden="true" data-star="n" data-id="${e.board.board_id}"></i>
                                </span>
                            </a>
                        `);
                    }
                }
            });
    </script>
@endsection
