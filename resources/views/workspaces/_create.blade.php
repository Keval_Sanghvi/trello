<div class="modal fade create-workspace-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document" data-backdrop="static">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Let's build a Workspace</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <p class="pl-3 pt-2">Boost your productivity by making it easier for everyone to access boards in one location.</p>
                </div>
            </div>
            <form action="{{ route('workspaces.store') }}" method="POST" id="create-workspace-form">
                @csrf
                <div class="modal-body">
                    <div class="form-group pb-2 @error('name') is-invalid @enderror">
                        <label for="name">Workspace name</label>
                        <input
                            type="text"
                            class="form-control"
                            id="name"
                            name="name"
                            value="{{ old('name') }}"
                            placeholder="Taco's Co."
                            data-error=".name_error">
                        <small class="d-block">This is the name of your company, team or organization.</small>
                        @error('name')
                            <small class="form-text text-danger">{{ $message }}</small>
                        @enderror
                        <small class="name_error text-danger h6"></small>
                    </div>

                    <div class="form-group pb-2 @error('type') is-invalid @enderror">
                        <label for="type">Workspace type</label>
                        <select name="type" id="type" class="form-control">
                            <option value="">Choose...</option>
                            @foreach($workspace_types as $workspace_type)
                                <option value="{{ $workspace_type }}">{{ $workspace_type }}</option>
                            @endforeach
                        </select>
                        @error('type')
                            <small class="form-text text-danger">{{ $message }}</small>
                        @enderror
                    </div>

                    <div class="form-group @error('description') is-invalid @enderror">
                        <label for="description">Workspace description <span class="font-weight-light">Optional</span></label>
                        <input
                            type="text"
                            class="form-control"
                            id="description"
                            name="description"
                            value="{{ old('description') }}"
                            placeholder="Our team organizes everything here."
                            data-error=".description_error">
                        <small>Get your members on board with a few words about your Workspace.</small>
                        @error('description')
                            <small class="form-text text-danger">{{ $message }}</small>
                        @enderror
                        <small class="description_error text-danger h6"></small>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="{{ asset('js/workspaces/create-validation.js') }}"></script>

<script>
    @error('name')
        $('.create-workspace-modal').modal('show');
    @enderror
    @error('description')
        $('.create-workspace-modal').modal('show');
    @enderror
    @error('type')
        $('.create-workspace-modal').modal('show');
    @enderror
</script>
