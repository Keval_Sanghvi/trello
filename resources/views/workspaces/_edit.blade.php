<div class="modal fade edit-workspace-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document" data-backdrop="static">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Your Workspace</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('workspaces.update', $workspace->id) }}" method="POST" id="edit-workspace-form">
                @csrf
                @method('PUT')
                <div class="modal-body">
                    <div class="form-group pb-2 @error('name') is-invalid @enderror">
                        <label for="name">Workspace name</label>
                        <input
                            type="text"
                            class="form-control"
                            id="name"
                            name="name"
                            value="{{ old('name', $workspace->name) }}"
                            placeholder="Taco's Co."
                            data-error=".name_error">
                        @error('name')
                            <small class="form-text text-danger">{{ $message }}</small>
                        @enderror
                        <small class="name_error text-danger h6"></small>
                    </div>

                    <div class="form-group pb-2 @error('type') is-invalid @enderror">
                        <label for="type">Workspace type</label>
                        <select name="type" id="type" class="form-control">
                            <option value="">Choose...</option>
                            @foreach($workspace_types as $workspace_type)
                                @if($workspace_type == old('type', $workspace->type))
                                    <option value="{{ $workspace_type }}" selected>{{ $workspace_type }}</option>
                                @else
                                    <option value="{{ $workspace_type }}">{{ $workspace_type }}</option>
                                @endif
                            @endforeach
                        </select>
                        @error('type')
                            <small class="form-text text-danger">{{ $message }}</small>
                        @enderror
                    </div>

                    <div class="form-group @error('description') is-invalid @enderror">
                        <label for="description">Workspace description <span class="font-weight-light">Optional</span></label>
                        <input
                            type="text"
                            class="form-control"
                            id="description"
                            name="description"
                            value="{{ old('description', $workspace->description) }}"
                            placeholder="Our team organizes everything here."
                            data-error=".description_error">
                        @error('description')
                            <small class="form-text text-danger">{{ $message }}</small>
                        @enderror
                        <small class="description_error text-danger h6"></small>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
