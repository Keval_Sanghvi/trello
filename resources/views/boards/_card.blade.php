<div class="modal fade card-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document" data-backdrop="static">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title">Card Details</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                   <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="d-flex">
                    <i class="fa fa-credit-card-alt mr-2" style="margin-top: 8px;" aria-hidden="true"></i>
                    <input id="card-name" type="text" name="card_name" class="form-control form-control-sm">
                </div>
                <p id="list-name" class="ml-4 pl-1 mt-1 mb-0"></p>
                <div class="d-flex mt-2">
                    <i class="fa fa-pencil-square-o mr-2" style="margin-top: 6px;" aria-hidden="true"></i>
                    <p class="m-0">Description</p>
                </div>
                <input id="card-description" type="text" name="card_description" class="form-control form-control-sm mt-2" placeholder="Add a more detailed description..." style="margin-left: 24px; width: 95%;">
                <hr>
                    <p class="mb-1 d-inline-block">Labels</p>
                    <div id="labels" class="mb-1"></div>
                    @can('update', $board)
                        <div class="dropdown">
                            <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" id="label-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                            </button>
                            <div class="dropdown-menu" style="width: 300px; padding: 15px; background: #efefef;" aria-labelledby="label-dropdown">
                                <div class="text-center pb-2 border-bottom">Labels</div>
                                <input type="text" id="label-name" name="label-name" class="form-control form-control-sm">
                                @foreach($label_colors as $label_color)
                                    <button class="label-background-select" onClick="labelColorSelection(event, this)" style="border: 0; background: {{ $label_color }}; width: 260px; height: 30px; margin: 5px 5px 5px 5px;"></button>
                                @endforeach
                                <small class="label_error text-danger h6 d-block"></small>
                                <button class="btn btn-primary mt-1" id="create-label">Save</button>
                            </div>
                        </div>
                    @endcan
                <hr>
                <div class="mb-3">
                    <h5>Card Members</h5>
                    <div class="card-members"></div>
                </div>
                @can('update', $board)
                    <form action="" method="POST" id="invite-card-form">
                        @csrf
                        <div class="form-group @error('card_users') is-invalid @enderror">
                            <select name="card_users[]" id="card_users" class="form-control select2" multiple="multiple" data-error=".card_users_error"></select>
                            <small class="card_users_error text-danger h6"></small>
                            @error('card_users')
                                <small class="form-text text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-sm btn-success">Invite to Card</button>
                    </form>
                @endcan
                <hr>
                @can('update', $board)
                    <p class="mb-1 d-inline-block">Due Date</p>
                    <input type="checkbox" id="status" name="status" class="ml-1 form-check-input">
                    <input type="datetime-local" class="form-control" name="due_date" id="due_date">
                    <button class="btn btn-success btn-sm mt-2" id="due_date-save">Save</button>
                @endcan
                <hr>
                <p class="mb-1 d-inline-block">Checklists</p>
                <div id="checklists"></div>
                @can('update', $board)
                    <div class="d-flex">
                        <input type="text" class="form-control" name="checklist_item" id="checklist_item">
                        <button class="btn btn-success btn-sm mt-2 ml-2" id="add-checklist">Add an item</button>
                    </div>
                @endcan
            </div>
        </div>
    </div>
</div>
