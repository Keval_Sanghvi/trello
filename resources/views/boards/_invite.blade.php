<div class="modal fade invite-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document" data-backdrop="static">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Invite to board</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="row p-3">
                <div class="col-12">
                    <form action="{{ route('boards.inviteMembers', $board->id) }}" method="POST" id="invite-form">
                        @csrf
                        <div class="form-group @error('users') is-invalid @enderror">
                            <select name="users[]" id="users" class="form-control select2" multiple="multiple" data-error=".users_error"></select>
                            <small class="users_error text-danger h6"></small>
                            @error('users')
                                <small class="form-text text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-sm btn-success">Invite to Board</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
