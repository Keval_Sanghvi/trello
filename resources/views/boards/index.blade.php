@extends('layouts.app')

@section('page-level-styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha512-SfTiTlX6kk+qitfevl/7LibUOeJWlt9rbyDn92a1DqWOw9vWG2MFoays0sgObmWazO5BQPiFucnnEAjpAB+/Sw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endsection

@section('main-content')
    <div class="container">
        <div class="row mt-4 mb-4">
           <div class="col-md-12">
                @include('layouts.partials._message')
           </div>
        </div>
        <div class="row mt-5 mb-5">
            @include('layouts.partials._sidebar')
            <div class="col-md-9 mt-2">
                <div class="favourites">
                    @if(! $favourites->isEmpty())
                        <h4 class="mb-3">
                            <i class="fa fa-star-o fa-lg mr-2"></i>
                            Starred Boards
                        </h4>
                        <div class="d-flex justify-content-start align-items-center flex-wrap mt-4 mb-4 favs" style="gap: 10px;">
                            @foreach($favourites as $favourite)
                                <a style="background: {{ $favourite->background }}; height: 100px; width: 180px;" class="rounded board deco-none" href="{{ route('boards.show', $favourite->id) }}">
                                    <h5 class="ml-2 mt-2 text-white">{{ $favourite->title }}</h5>
                                    <span>
                                        <i class="fa fa-star text-warning float-right star" style="position: relative; top: 38px; left: -10px;" aria-hidden="true" data-star="y" data-id="{{ $favourite->id }}"></i>
                                    </span>
                                </a>
                            @endforeach
                        </div>
                    @endif
                </div>
                @if(! $personalBoards->isEmpty())
                    <h4 class="mt-4">
                        <span style="background: linear-gradient(#b22865, #cd5a91); padding: .3rem .7rem;" class="rounded text-white">P</span>
                        Personal Boards
                    </h4>
                    <div class="d-flex justify-content-start align-items-center flex-wrap mt-4 mb-4 pers" style="gap: 10px;">
                        @foreach($personalBoards as $personalBoard)
                            <a href="{{ route('boards.show', $personalBoard->id) }}" style="background: {{ $personalBoard->background }}; height: 100px; width: 180px;" class="deco-none rounded board @if(! $personalBoard->isFavourite()) fav @endif">
                                <h5 class="ml-2 mt-2 text-white">{{ $personalBoard->title }}</h5>
                                @if($personalBoard->isFavourite() == 1)
                                    <span>
                                        <i class="fa fa-star text-warning float-right star" style="position: relative; top: 38px; left: -10px;" aria-hidden="true" data-star="y" data-id="{{ $personalBoard->id }}"></i>
                                    </span>
                                @else
                                    <span class="d-none">
                                        <i class="fa fa-star-o text-warning float-right star" style="position: relative; top: 38px; left: -10px;" aria-hidden="true" data-star="n" data-id="{{ $personalBoard->id }}"></i>
                                    </span>
                                @endif
                            </a>
                        @endforeach
                        <button style="height: 100px; width: 180px;" class="rounded btn btn-light personalBoardButton" data-toggle="modal" data-target=".create-board-modal">
                            <p class="m-0 text-dark">Create new board</p>
                        </button>
                    </div>
                @endif
                @if(! $workspaces->isEmpty())
                    <h5 class="text-uppercase text-muted mt-5">
                        Your Workspaces
                    </h5>
                    <div class="mt-4 mb-4">
                        @foreach($workspaces as $workspace)
                            <p class="font-weight-bold">
                                <span style="background: linear-gradient(#cc4223, #cb7d25); padding: .3rem .7rem;" class="rounded text-white mr-2">{{ $workspace->name[0] }}</span>
                                {{ $workspace->name }}
                            </p>
                            @if($workspace->boards->isEmpty())
                                <div class="row shadow-sm pt-2 pb-2 m-0">
                                    <div class="col-md-3">
                                        <img src="{{ asset('images/workspace.svg') }}" alt="">
                                    </div>
                                    <div class="col-md-9">
                                        <p class="h6 mt-4">Welcome to the Workspace!</p>
                                        <p>This workspace contains no boards!</p>
                                    </div>
                                </div>
                            @endif
                            <div class="d-flex justify-content-start align-items-center flex-wrap mt-4 mb-4 wors" style="gap: 10px;">
                                @foreach($workspace->boards as $board)
                                    <a href="{{ route('boards.show', $board->id) }}" style="background: {{ $board->background }}; height: 100px; width: 180px;" class="deco-none rounded board @if(! $board->isFavourite()) fav @endif">
                                        <h5 class="ml-2 mt-2 text-white">{{ $board->title }}</h5>
                                        @if($board->isFavourite())
                                            <span>
                                                <i class="fa fa-star text-warning float-right star" style="position: relative; top: 38px; left: -10px;" aria-hidden="true" data-star="y" data-id="{{ $board->id }}"></i>
                                            </span>
                                        @else
                                            <span class="d-none">
                                                <i class="fa fa-star-o text-warning float-right star" style="position: relative; top: 38px; left: -10px;" aria-hidden="true" data-star="n" data-id="{{ $board->id }}"></i>
                                            </span>
                                        @endif
                                    </a>
                                @endforeach
                                <button onClick="selectOption('{{ $workspace->id }}')" style="height: 100px; width: 180px;" class="rounded btn btn-light workspaceButton-{{$workspace->id}}" data-toggle="modal" data-target=".create-board-modal">
                                    <p class="m-0 text-dark">Create new board</p>
                                </button>
                            </div>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection

@section('page-level-scripts')
    <script>
        function selectOption(workspace_id) {
            $("#select-workspace").val(workspace_id);
        }
    </script>

    <script>
        $(document).on("mouseenter", ".board", function() {
            $(this).css('cursor', 'pointer');
            if($(this).hasClass('fav')) {
                $(this).children().last().removeClass("d-none");
            }
        });

        $(document).on("mouseleave", ".board", function() {
            $(this).css('cursor', 'default');
            if($(this).hasClass('fav')) {
                $(this).children().last().addClass("d-none");
            }
        });
    </script>

    <script>
        $(".row").on('click', '.star', function(e) {
            e.preventDefault();
            let id = $(this).data("id");
            let mark = $(this).data("star");
            let background = RGBToHex($(this).parent().parent().css('background-color'));
            let title = $(this).parent().siblings('h5').text();
            $.ajax({
                type: 'POST',
                url: `{{ route('boards.changeFavouriteStatus') }}`,
                data: {_token: '{{ csrf_token() }}', 'board_id': id, 'mark': mark },
                success: function(data) {
                    if(data) {
                        if(mark.includes("n")) {
                            if($(".favourites .board").length == 0) {
                                $(".favourites").append(`
                                    <h4 class="mb-3">
                                        <i class="fa fa-star-o fa-lg mr-2"></i>
                                        Starred Boards
                                    </h4>
                                    <div class="d-flex justify-content-start align-items-center flex-wrap mt-4 mb-4 favs" style="gap: 10px;">
                                        <a href="boards/${id}" style="background: ${background}; height: 100px; width: 180px;" class="rounded board deco-none">
                                            <h5 class="ml-2 mt-2 text-white">${title}</h5>
                                            <span>
                                                <i class="fa fa-star text-warning float-right star" style="position: relative; top: 38px; left: -10px;" aria-hidden="true" data-star="y" data-id="${id}"></i>
                                            </span>
                                        </a>
                                    </div>
                                `);
                            } else {
                                $(".favourites .favs").append(`
                                    <a href="boards/${id}" style="background: ${background}; height: 100px; width: 180px;" class="rounded board deco-none">
                                        <h5 class="ml-2 mt-2 text-white">${title}</h5>
                                        <span>
                                            <i class="fa fa-star text-warning float-right star" style="position: relative; top: 38px; left: -10px;" aria-hidden="true" data-star="y" data-id="${id}"></i>
                                        </span>
                                    </a>
                                `);
                            }
                            $(`.board [data-id=${id}]`).attr("data-star", "y");
                            $(`.board [data-id=${id}]`).data("star", "y");
                            $(`.board [data-id=${id}]`).removeClass("fa-star-o");
                            $(`.board [data-id=${id}]`).addClass("fa-star");
                            $(`.board [data-id=${id}]`).parent().removeClass("d-none");
                            $(`.board [data-id=${id}]`).parent().parent().removeClass("fav");
                            $(`.board [data-id=${id}]`).parent().parent().off();
                        } else {
                            $(`.favourites .board [data-id=${id}]`).parent().parent().remove();
                            if($(".favourites .board").length == 0) {
                                $(".favourites").empty();
                            }
                            $(`.board [data-id=${id}]`).attr("data-star", "n");
                            $(`.board [data-id=${id}]`).data("star", "n");
                            $(`.board [data-id=${id}]`).removeClass("fa-star");
                            $(`.board [data-id=${id}]`).addClass("fa-star-o");
                            $(`.board [data-id=${id}]`).parent().addClass("d-none");
                            $(`.board [data-id=${id}]`).parent().parent().addClass("fav");
                            $(`.board [data-id=${id}]`).parent().parent().bind({
                                mouseenter: function() {
                                    $(this).children().last().removeClass("d-none");
                                    $(this).css('cursor', 'pointer');
                                },
                                mouseleave: function() {
                                    $(this).children().last().addClass("d-none");
                                    $(this).css('cursor', 'default');
                                }
                            });
                        }
                    }
                },
                error: function(data) {
                    if(data.status == 401 || data.status == 403) {
                        let url = "{{ route('home') }}";
                        document.location.href = url;
                    }
                }
            });
        });
    </script>
@endsection

@section('broadcasting-scripts')
    <script>
        let auth = {{ auth()->id() }};
        let boards = {{ json_encode(auth()->user()->boards->pluck('id')->toArray()) }};
        for(var i = 0; i < boards.length; i++) {
            Echo.private(`board-update.${boards[i]}`)
                .listen('BoardUpdateEvent', (e) => {
                    if(auth != e.board.auth) {
                        $(`.favs [data-id=${e.board.id}]`).parent().siblings("h5").text(e.board.title);
                        $(`.pers [data-id=${e.board.id}]`).parent().siblings("h5").text(e.board.title);
                        $(`.wors [data-id=${e.board.id}]`).parent().siblings("h5").text(e.board.title);
                        $(`.favs [data-id=${e.board.id}]`).parent().parent().css('background-color', e.board.background);
                        $(`.pers [data-id=${e.board.id}]`).parent().parent().css('background-color', e.board.background);
                        $(`.wors [data-id=${e.board.id}]`).parent().parent().css('background-color', e.board.background);
                    }
                });
        }

        Echo.private(`board.${auth}`)
            .listen('BoardEvent', (e) => {
                let workspace_id = e.board.workspace_id;
                if(e.board.feature == "add") {
                    $(".personalBoardButton").before(`
                        <a href="/boards/${e.board.board_id}" style="background: ${e.board.background}; height: 100px; width: 180px;" class="deco-none rounded board fav">
                            <h5 class="ml-2 mt-2 text-white">${e.board.title}</h5>
                            <span class="d-none">
                                <i class="fa fa-star-o text-warning float-right star" style="position: relative; top: 38px; left: -10px;" aria-hidden="true" data-star="n" data-id="${e.board.board_id}"></i>
                            </span>
                        </a>
                    `);
                }
            });
    </script>
@endsection
