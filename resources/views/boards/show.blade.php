@extends('layouts.app')

@section('page-level-styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha512-SfTiTlX6kk+qitfevl/7LibUOeJWlt9rbyDn92a1DqWOw9vWG2MFoays0sgObmWazO5BQPiFucnnEAjpAB+/Sw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />

    <style>
        body {
            background: {{ $board->background }};
        }

        .background-primary {
            background: @if($board->background == "#0079bf")
                            {{ $navbar_backgrounds[0] }};
                        @elseif($board->background == "#d29034")
                            {{ $navbar_backgrounds[1] }};
                        @elseif($board->background == "#519839")
                            {{ $navbar_backgrounds[2] }};
                        @elseif($board->background == "#b04632")
                            {{ $navbar_backgrounds[3] }};
                        @elseif($board->background == "#89609e")
                            {{ $navbar_backgrounds[4] }};
                        @elseif($board->background == "#cd5a91")
                            {{ $navbar_backgrounds[5] }};
                        @elseif($board->background == "#4bbf6b")
                            {{ $navbar_backgrounds[6] }};
                        @elseif($board->background == "#00aecc")
                            {{ $navbar_backgrounds[7] }};
                        @elseif($board->background == "#838c91")
                            {{ $navbar_backgrounds[8] }};
                        @endif;
        }

        span.select2 {
            width: 100% !important;
        }

        .show-menu.dropdown-toggle:after {
            display: none !important;
        }

        .ui-sortable-placeholder {
            visibility: inherit !important;
            background: transparent;
            border: #666 2px dashed;
        }

        .dropdown-toggle::after {
            display: none;
        }
    </style>
@endsection

@section('main-content')
    <div class="row ml-0 mr-0 mt-2">
        <div class="col-md-1">
            @can('update', $board)
                <input id="title" type="text" name="newTitle" value="{{ $board->title }}" class="form-control" style="height: 40px;">
            @else
                <button class="btn btn-light" style="height: 40px; width: 100px;">{{ $board->title }}</button>
            @endcan
        </div>
        @can('hasBoard', $board)
            <a class="deco-none d-inline-block p-2 mark rounded" href="" id="markAsFav">
                <i style="margin-top: 4px;" class="fa @if($board->isFavourite()) fa-star @else fa-star-o @endif text-warning float-right" aria-hidden="true" data-star="@if($board->isFavourite()) y @else n @endif" data-id="{{ $board->id }}"></i>
            </a>
        @endcan
        <div class="ml-3">
            @foreach($users as $user)
                <img src="{{ $user->avatar }}" alt="Avatar" class="margin-left: -5px;">
            @endforeach
            @if($totalUsers > 0)
                <div style="height: 40px; width: 40px; background: #ccc; border-radius: 50%; vertical-align: bottom; text-align: center; padding-top: 7px;" class="d-inline-block">
                    +{{ $totalUsers }}
                </div>
            @endif
        </div>
        @can('invite', $board)
            <div class="col-md-1">
                <a class="deco-none btn btn-light" href="#" data-toggle="modal" data-target=".invite-modal" style="height: 40px; line-height: 25px;">
                    Invite
                </a>
            </div>
        @endcan
        @can('update', $board)
            <div class="dropdown ml-auto mr-3">
                <a style="height: 40px; line-height: 25px;" class="btn btn-light show-menu dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-ellipsis-h mr-2" aria-hidden="true"></i>
                    Show menu
                </a>
                <div style="left: -200px !important; width: 300px;" class="dropdown-menu p-2" aria-labelledby="dropdownMenuLink">
                    <div class="text-center">
                        <h5>Change Background</h5>
                        <div class="d-flex justify-content-center flex-wrap colors">
                            @foreach($board_backgrounds as $board_background)
                                <button class="btn change-background" style="border: 0; background: {{ $board_background }}; width: 75px; height: 75px; margin: 5px;"></button>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        @endcan
    </div>

    <div class="d-flex p-3 pt-4 align-items-start" id="lists" style="gap: 15px; height: 87vh; overflow-y: auto;">
        @foreach($lists as $list)
            <div class="list p-2 rounded" style="background: #ebecf0; width: 250px; flex-shrink: 0;" data-id="{{ $list->id }}">
                <input type="text" name="list_name" class="form-control form-control-sm mb-2 list-name" value="{{ $list->name }}" data-id="{{ $list->id }}">
                <div class="cards sortable ui-sortable" style="min-height: 10px;" data-id="{{ $list->id }}">
                    @foreach($list->cards as $card)
                        <div class="card mb-2 ui-sortable-handle" id="{{ $card->id }}" data-id="{{ $card->id }}">
                            <div class="card-body p-2">
                                <div class="card-labels">
                                    @foreach($card->getLabels() as $label)
                                        <span class="badge text-white" id="{{ $label->id }}" style="background: {{ $label->color }}">{{ $label->name }}</span>
                                    @endforeach
                                </div>
                                <p class="m-0">{{ $card->name }}</p>
                                @if($card->status == 'overdue')
                                    <span class="badge badge-pill badge-danger">Overdue</span>
                                @elseif($card->status == 'completed')
                                    <span class="badge badge-pill badge-success">Completed</span>
                                @endif
                            </div>
                        </div>
                    @endforeach
                </div>
                @can('update', $board)
                    <button class="create-card p-2 btn btn-light btn-sm text-muted" id="add-card-{{ $list->id }}">
                        <i class="fa fa-plus mr-2"></i>
                        Add a card
                    </button>
                    <div class="add-card d-none">
                        <input type="text" class="form-control" name="card" placeholder="Enter a title for this card..." autocomplete="off">
                        <button class="btn btn-primary mt-1 btn-sm add-card-button" id="add-card-to-list-{{ $list->id }}" data-id="{{ $list->id }}">Add card</button>
                        <button class="btn mt-1 btn-sm card-cross" style="background: transparent;" id="cross-card-{{ $list->id }}">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </button>
                    </div>
                @endcan
            </div>
        @endforeach
        @can('update', $board)
            <div class="create-list-button" style="flex-shrink: 0;">
                <button class="create-list p-2 btn btn-light" style="opacity: 0.9;">
                    <i class="fa fa-plus mr-2"></i>
                    Add a list
                </button>
                <div class="add-list p-2 d-none" style="background: #ccc;">
                    <input type="text" class="form-control" name="list" placeholder="Enter list title..." autocomplete="off">
                    <button class="btn btn-primary mt-1 btn-sm" id="add">Add list</button>
                    <button class="btn mt-1 btn-sm" style="background: transparent;" id="cross">
                        <i class="fa fa-times" aria-hidden="true"></i>
                    </button>
                </div>
            </div>
        @endcan
    </div>

    @include('boards._invite')
    @include('boards._card')
@endsection

@section('page-level-scripts')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

    @can('invite', $board)
        <script>
            @error('users')
                $('.invite-modal').modal('show');
            @enderror

            $("#invite-form").validate({
                ignore: [],
                rules: {
                    'users[]': {
                        required: true
                    }
                },
                errorElement: 'small',
                errorPlacement: function(error, element) {
                    var placement = $(element).data('error');
                    if (placement) {
                        $(placement).append(error)
                    } else {
                        error.addClass('text-danger');
                        error.insertAfter(element);
                    }
                }
            });

            @error('card_users')
                $('.invite-modal').modal('show');
            @enderror

            $("#invite-card-form").validate({
                ignore: [],
                rules: {
                    'card_users[]': {
                        required: true
                    }
                },
                errorElement: 'small',
                errorPlacement: function(error, element) {
                    var placement = $(element).data('error');
                    if (placement) {
                        $(placement).append(error)
                    } else {
                        error.addClass('text-danger');
                        error.insertAfter(element);
                    }
                }
            });
        </script>
    @endcan

    <script>
        @can('update', $board)
            $('#users').select2({
                allowClear: true,
                placeholder: 'Email Address',
                ajax: {
                    url: '/autocomplete-search-board',
                    dataType: 'json',
                    delay: 250,
                    data: function(params) {
                        return {
                            q: $.trim(params.term),
                            board_id: {{ $board->id }}
                        };
                    },
                    processResults: function(data) {
                        return {
                            results: $.map(data, function(item) {
                                return {
                                    text: item.email,
                                    id: item.id
                                }
                            })
                        };
                    }
                }
            });

            $('#card_users').select2({
                allowClear: true,
                placeholder: 'Email Address',
                ajax: {
                    url: '/autocomplete-search-card',
                    dataType: 'json',
                    delay: 250,
                    data: function(params) {
                        var card_id = $(this).data('id');
                        return {
                            q: $.trim(params.term),
                            board_id: {{ $board->id }},
                            card_id: card_id,
                        };
                    },
                    processResults: function(data) {
                        return {
                            results: $.map(data, function(item) {
                                return {
                                    text: item.email,
                                    id: item.id
                                }
                            })
                        };
                    }
                }
            });
        @endcan

        $("#markAsFav").click(function(e) {
            e.preventDefault();
            let id = $("#markAsFav i").data("id");
            let mark = $("#markAsFav i").data("star");
            $.ajax({
                type: 'POST',
                url: `{{ route('boards.changeFavouriteStatus') }}`,
                data: {_token: '{{ csrf_token() }}', 'board_id': id, 'mark': mark },
                success: function(data) {
                    if(data) {
                        if(mark.includes("n")) {
                            $("#markAsFav i").addClass("fa-star");
                            $("#markAsFav i").removeClass("fa-star-o");
                            $("#markAsFav i").attr("data-star", "y");
                            $("#markAsFav i").data("star", "y");
                        } else {
                            $("#markAsFav i").addClass("fa-star-o");
                            $("#markAsFav i").removeClass("fa-star");
                            $("#markAsFav i").attr("data-star", "n");
                            $("#markAsFav i").data("star", "n");
                        }
                    }
                },
                error: function(data) {
                    if(data.status == 401 || data.status == 403) {
                        let url = "{{ route('home') }}";
                        document.location.href = url;
                    }
                }
            });
        });

        @can('update', $board)
            $("#title").focusout(function(e) {
                let newTitle = $(this).val();
                if(newTitle == "") {
                    return;
                }
                $.ajax({
                    type: 'POST',
                    url: `{{ route('boards.changeTitle') }}`,
                    data: {_token: '{{ csrf_token() }}', 'board_id': {{ $board->id }}, 'newTitle': newTitle },
                    success: function(data) {
                        if(data) {

                        }
                    },
                    error: function(data) {
                        if(data.status == 401 || data.status == 403) {
                            let url = "{{ route('home') }}";
                            document.location.href = url;
                        }
                    }
                });
            });

            $(".colors").on('click', 'button', function() {
                var color = RGBToHex($(this).css("background-color"));
                $.ajax({
                    type: 'POST',
                    url: `{{ route('boards.changeBackground') }}`,
                    data: {_token: '{{ csrf_token() }}', 'board_id': {{ $board->id }}, 'newBackground': color },
                    success: function(data) {
                        if(data) {
                            $("body").css("background-color", color);
                            var navbar_color = "";
                            if(data.background == "#0079bf") {
                                navbar_color = '{{ $navbar_backgrounds[0] }}';
                            } else if(data.background == "#d29034") {
                                navbar_color = '{{ $navbar_backgrounds[1] }}';
                            } else if(data.background == "#519839") {
                                navbar_color = '{{ $navbar_backgrounds[2] }}';
                            } else if(data.background == "#b04632") {
                                navbar_color = '{{ $navbar_backgrounds[3] }}';
                            } else if(data.background == "#89609e") {
                                navbar_color = '{{ $navbar_backgrounds[4] }}';
                            } else if(data.background == "#cd5a91") {
                                navbar_color = '{{ $navbar_backgrounds[5] }}';
                            } else if(data.background == "#4bbf6b") {
                                navbar_color = '{{ $navbar_backgrounds[6] }}';
                            } else if(data.background == "#00aecc") {
                                navbar_color = '{{ $navbar_backgrounds[7] }}';
                            } else if(data.background == "#838c91") {
                                navbar_color = '{{ $navbar_backgrounds[8] }}';
                            }
                            $(".background-primary").css('background-color', navbar_color);
                        }
                    },
                    error: function(data) {
                        if(data.status == 401 || data.status == 403) {
                            location.reload();
                        }
                    }
                });
            });
        @endcan
    </script>

    <script>
        @can('update', $board)
            function hooks() {
                $('.cards').sortable({
                    connectWith : ".sortable",
                    receive : function(e, ui) {
                        var list_id = $(ui.item).parent(".sortable").data("id");
                        var card_id = $(ui.item).data("id");
                        $.ajax({
                            type: 'POST',
                            url: `{{ route('cards.changeList') }}`,
                            data: {_token: '{{ csrf_token() }}', 'list_id': list_id, 'card_id': card_id, 'board_id': {{ $board->id }}},
                            success: function(data) {
                                if(data) {

                                }
                            },
                            error: function(data) {
                                if(data.status == 401 || data.status == 403) {
                                    let url = "{{ route('home') }}";
                                    document.location.href = url;
                                }
                            }
                        });
                    }
                }).disableSelection();
            }
            hooks();

            $(".create-list").click(function() {
                $(".add-list").removeClass("d-none");
                $(".create-list").addClass("d-none");
                $(".add-list input[type='text']").val("");
                $(".add-list input[type='text']").focus();
            });

            $("#cross").on('click', function() {
                $(".add-list").addClass("d-none");
                $(".create-list").removeClass("d-none");
            });

            $("#add").click(function() {
                var value = $(".add-list input[type='text']").val();
                if(value == "") {
                    return;
                }
                $.ajax({
                    type: 'POST',
                    url: `{{ route('lists.store') }}`,
                    data: {_token: '{{ csrf_token() }}', 'board_id': {{ $board->id }}, 'name': value},
                    success: function(data) {
                        if(data) {
                            $(".create-list-button").before(`
                                <div class="list p-2 rounded" style="background: #ebecf0; width: 250px; flex-shrink: 0;" data-id="${data.list_id}">
                                    <input type="text" name="list_name" class="form-control form-control-sm mb-2 list-name" value="${value}" data-id="${data.list_id}">
                                    <div class="cards sortable ui-sortable" style="min-height: 10px;" data-id="${data.list_id}"></div>
                                    <button class="create-card p-2 btn btn-light btn-sm text-muted" id="add-card-${data.list_id}">
                                        <i class="fa fa-plus mr-2"></i>
                                        Add a card
                                    </button>
                                    <div class="add-card d-none">
                                        <input type="text" class="form-control" name="card" placeholder="Enter a title for this card..." autocomplete="off">
                                        <button class="btn btn-primary mt-1 btn-sm add-card-button" id="add-card-to-list-${data.list_id}" data-id="${data.list_id}">Add card</button>
                                        <button class="btn mt-1 btn-sm card-cross" style="background: transparent;" id="cross-card-${data.list_id}">
                                            <i class="fa fa-times" aria-hidden="true"></i>
                                        </button>
                                    </div>
                                </div>
                            `);
                            hooks();
                            $(".add-list input[type='text']").val("");
                            $(".add-list input[type='text']").focus();
                        }
                    },
                    error: function(data) {
                        if(data.status == 401 || data.status == 403) {
                            let url = "{{ route('home') }}";
                            document.location.href = url;
                        }
                    }
                });
            });

            $("#lists").on('click', '.add-card-button', function() {
                var value = $(this).siblings('input[type="text"]').val();
                if(value == "") {
                    return;
                }
                var list_id = $(this).data("id");
                $.ajax({
                    type: 'POST',
                    url: `{{ route('cards.store') }}`,
                    data: {_token: '{{ csrf_token() }}', 'board_id': {{ $board->id }}, 'list_id': list_id, 'name': value},
                    success: function(data) {
                        if(data) {
                            $(`.list[data-id='${data.list_id}'`).children('.cards').append(`
                                <div class="card mb-2 ui-sortable-handle" id="${data.card_id}" data-id="${data.card_id}">
                                    <div class="card-body p-2">
                                        <p class="m-0">${data.card}</p>
                                    </div>
                                </div>
                            `);
                            $(".add-card").addClass('d-none');
                            $(".create-card").removeClass('d-none');
                        }
                    },
                    error: function(data) {
                        if(data.status == 401 || data.status == 403) {
                            let url = "{{ route('home') }}";
                            document.location.href = url;
                        }
                    }
                });
            });

            $("#lists").on('click', '.create-card', function() {
                $(".add-card").addClass('d-none');
                $(".create-card").removeClass('d-none');
                $(this).siblings('.add-card').removeClass('d-none');
                $(this).addClass("d-none");
                $(this).siblings(".add-card").children("input[type='text']").val("");
                $(this).siblings(".add-card").children("input[type='text']").focus();
            });

            $(".add-card").on('click', '.card-cross', function() {
                $(this).parent().addClass("d-none");
                $(this).parent().siblings('.create-card').removeClass("d-none");
            });
        @endcan

        $("#lists").on('click', '.card', function() {
            var id = $(this).data("id");
            $.ajax({
                type: 'POST',
                url: `{{ route('cards.getCard') }}`,
                data: {_token: '{{ csrf_token() }}', 'card_id': id},
                success: function(data) {
                    if(data) {
                        $('.card-modal').modal('show');
                        $(".card-members").empty();
                        $('.card-modal #card-name').val(data.name);
                        $('.card-modal #card-description').val(data.description);
                        $('.card-modal #card-name').data('id', id);
                        $('.card-modal #card-name').attr('data-id', id);
                        $('.card-modal #card-description').data('id', id);
                        $('.card-modal #card-description').attr('data-id', id);
                        $('.card-modal #list-name').html("in list " + data.list_name);
                        $('#invite-card-form').attr('action', `/boards/{{ $board->id }}/cards/${id}/invite-members`);
                        $('#invite-card-form #card_users').attr('data-id', id);
                        $('.card-modal #due_date').attr('data-id', id);
                        $('.card-modal #due_date').data('id', id);
                        $('.card-modal #status').attr('data-id', id);
                        $('.card-modal #status').data('id', id);
                        $('.card-modal #create-label').attr('data-id', id);
                        $('.card-modal #create-label').data('id', id);
                        $('.card-modal #checklist_item').attr('data-id', id);
                        $('.card-modal #checklist_item').data('id', id);
                        $('.card-modal #due_date').val('');
                        $(".label_error").html("");
                        $(".card-modal .dropdown-menu .label-background-select").each(function() {
                            $(this).text("");
                        });
                        $('.card-modal #create-label').attr('data-color', '');
                        $('.card-modal #create-label').data('color', '');
                        $("#due_date").attr("min", new Date().toISOString().slice(0, 19));
                        if(! $("#status").length) {
                            $('<input type="checkbox" id="status" name="status" class="ml-1 form-check-input">').insertBefore("#due_date");
                            $('.card-modal #status').attr('data-id', id);
                            $('.card-modal #status').data('id', id);
                        }
                        $("#status").prop("checked", false);
                        $(".card-modal .badge").remove();
                        if(data.status == "completed") {
                            $("#status").prop("checked", true);
                        }
                        if(data.due_date != null) {
                            var due_date = data.due_date.slice(0, 16).replace(' ', 'T');
                            $('.card-modal #due_date').val(due_date);
                            var today = new Date().toISOString().slice(0, 19).replace('T', ' ');
                            if(today > data.due_date) {
                                $("#status").remove();
                                $('<span class="badge badge-pill badge-danger">Overdue</span>').insertBefore("#due_date");
                                $.ajax({
                                    type: 'POST',
                                    url: `{{ route('cards.changeStatus') }}`,
                                    data: {_token: '{{ csrf_token() }}', 'board_id': {{ $board->id }}, 'card_id': id, 'status': 'overdue' },
                                    success: function(data) {
                                        if(data) {

                                        }
                                    },
                                    error: function(data) {
                                        if(data.status == 401 || data.status == 403) {
                                            let url = "{{ route('home') }}";
                                            document.location.href = url;
                                        }
                                    }
                                });
                            }
                        } else {
                            $("#status").remove();
                        }
                        data.members.forEach(member => $(".card-members").append(`
                            <img src="${member}" alt="Avatar" class="margin-left: -5px;">
                        `));
                        data.labels.forEach(label => $("#labels").append(`
                            <span class="badge text-white" id="${label.id}" style="background: ${label.color}">${label.name}</span>
                        `));
                        $("#checklists").empty();
                        @can('update', $board)
                            data.checklists.forEach(checklist => $("#checklists").append(`
                                <div>
                                    <input type="checkbox" id="checklist-${checklist.id}" data-id="${checklist.id}" name="checklist-${checklist.id}" class="ml-1 form-check-input checklist-class">
                                    <span class="m-0 mb-1 ml-4">${checklist.name}</span>
                                </div>
                            `));
                        @else
                            data.checklists.forEach(checklist => $("#checklists").append(`
                                <div>
                                    <span class="m-0 mb-1">${checklist.name}</span>
                                </div>
                            `));
                        @endcan
                        $(".removeChecklists").remove();
                        @can('update', $board)
                            if(data.checklists.length > 0) {
                                $("#checklists").before(`<button class="btn btn-sm btn-danger removeChecklists" id="removeChecklists-${id}" data-id="${id}"><i class="fa fa-trash" aria-hidden="true"></i></button>`);
                            }
                        @endcan
                        if(data.totalUsers >= 1) {
                            $(".card-members").append(`
                                <div style="height: 40px; width: 40px; background: #ccc; border-radius: 50%; vertical-align: bottom; text-align: center; padding-top: 7px;" class="d-inline-block">
                                    +${data.totalUsers}
                                </div>
                            `);
                        }
                    }
                },
                error: function(data) {
                    if(data.status == 401 || data.status == 403) {
                        let url = "{{ route('home') }}";
                        document.location.href = url;
                    }
                }
            });
        });

        @can('update', $board)
            $("#lists").on('focusout', '.list-name', function(e) {
                let newName = $(this).val();
                if(newName == "") {
                    return;
                }
                var id = $(this).data("id");
                $.ajax({
                    type: 'POST',
                    url: `{{ route('lists.changeName') }}`,
                    data: {_token: '{{ csrf_token() }}', 'list_id': id, 'newName': newName, 'board_id': {{ $board->id }} },
                    success: function(data) {
                        if(data) {

                        }
                    },
                    error: function(data) {
                        if(data.status == 401 || data.status == 403) {
                            let url = "{{ route('home') }}";
                            document.location.href = url;
                        }
                    }
                });
            });

            $("#card-name").focusout(function(e) {
                let newName = $(this).val();
                if(newName == "") {
                    return;
                }
                var id = $(this).data("id");
                $.ajax({
                    type: 'POST',
                    url: `{{ route('cards.changeName') }}`,
                    data: {_token: '{{ csrf_token() }}', 'board_id': {{ $board->id }}, 'card_id': id, 'newName': newName },
                    success: function(data) {
                        if(data) {
                            $(`.card#${id}`).find('p').html(newName);
                        }
                    },
                    error: function(data) {
                        if(data.status == 401 || data.status == 403) {
                            let url = "{{ route('home') }}";
                            document.location.href = url;
                        }
                    }
                });
            });

            $("#card-description").focusout(function(e) {
                let newDescription = $(this).val();
                if(newDescription == "") {
                    return;
                }
                var id = $(this).data("id");
                $.ajax({
                    type: 'POST',
                    url: `{{ route('cards.changeDescription') }}`,
                    data: {_token: '{{ csrf_token() }}', 'board_id': {{ $board->id }}, 'card_id': id, 'newDescription': newDescription },
                    success: function(data) {
                        if(data) {

                        }
                    },
                    error: function(data) {
                        if(data.status == 401 || data.status == 403) {
                            let url = "{{ route('home') }}";
                            document.location.href = url;
                        }
                    }
                });
            });

            $("#due_date-save").click(function(e) {
                let date = $(this).siblings('#due_date').val();
                if(date == "") {
                    return;
                }
                var id = $(this).siblings('#due_date').data("id");
                let db_date = new Date(date).toISOString().slice(0, 19).replace('T', ' ');
                $.ajax({
                    type: 'POST',
                    url: `{{ route('cards.changeDueDate') }}`,
                    data: {_token: '{{ csrf_token() }}', 'board_id': {{ $board->id }}, 'card_id': id, 'due_date': db_date },
                    success: function(data) {
                        if(data) {
                            $('<input type="checkbox" id="status" name="status" class="ml-1 form-check-input">').insertBefore("#due_date");
                            $('.card-modal #status').attr('data-id', id);
                            $('.card-modal #status').data('id', id);
                        }
                    },
                    error: function(data) {
                        if(data.status == 401 || data.status == 403) {
                            let url = "{{ route('home') }}";
                            document.location.href = url;
                        }
                    }
                });
            });

            $(".card-modal").on('click', '#status', function(e) {
                let isChecked = $('#status').is(':checked');
                var id = $(this).data("id");
                var status = isChecked == true ? 'completed' : 'working';
                $.ajax({
                    type: 'POST',
                    url: `{{ route('cards.changeStatus') }}`,
                    data: {_token: '{{ csrf_token() }}', 'board_id': {{ $board->id }}, 'card_id': id, 'status': status },
                    success: function(data) {
                        if(data) {
                            if(status == 'completed') {
                                $(`.card#${id}`).find('.badge').remove();
                                $(`.card#${id}`).find('p').after('<span class="badge badge-pill badge-success">Completed</span>');
                            } else {
                                $(`.card#${id}`).find('.badge').remove();
                            }
                        }
                    },
                    error: function(data) {
                        if(data.status == 401 || data.status == 403) {
                            let url = "{{ route('home') }}";
                            document.location.href = url;
                        }
                    }
                });
            });

            $("#add-checklist").click(function(e) {
                let item = $(this).siblings('#checklist_item').val();
                if(item == "") {
                    return;
                }
                var id = $(this).siblings('#checklist_item').data("id");
                $.ajax({
                    type: 'POST',
                    url: `{{ route('checklists.store') }}`,
                    data: {_token: '{{ csrf_token() }}', 'board_id': {{ $board->id }}, 'card_id': id, 'name': item },
                    success: function(data) {
                        if(data) {
                            $('#checklist_item').val("");
                            $("#checklists").append(`
                                <div>
                                    <input type="checkbox" id="checklist-${data.checklist_id}" data-id="${data.checklist_id}" name="checklist-${data.checklist_id}" class="ml-1 form-check-input checklist-class">
                                    <span class="m-0 mb-1 ml-4">${item}</span>
                                </div>
                            `);
                            if(! $('.removeChecklists').length) {
                                $("#checklists").before(`<button class="btn btn-sm btn-danger removeChecklists" id="removeChecklists-${id}" data-id="${id}"><i class="fa fa-trash" aria-hidden="true"></i></button>`);
                            }
                        }
                    },
                    error: function(data) {
                        if(data.status == 401 || data.status == 403) {
                            let url = "{{ route('home') }}";
                            document.location.href = url;
                        }
                    }
                });
            });

            $("#checklists").on('click', '.checklist-class', function() {
                let checklist_id = $(this).data("id");
                var isChecked = $(this).is(':checked');
                var status = isChecked == true ? 'complete' : 'incomplete';
                $.ajax({
                    type: 'POST',
                    url: `{{ route('checklists.changeStatus') }}`,
                    data: {_token: '{{ csrf_token() }}', 'board_id': {{ $board->id }}, 'checklist_id': checklist_id, 'status': status },
                    success: function(data) {
                        if(data) {

                        }
                    },
                    error: function(data) {
                        if(data.status == 401 || data.status == 403) {
                            let url = "{{ route('home') }}";
                            document.location.href = url;
                        }
                    }
                });
            });

            $(".card-modal").on('click', '.removeChecklists', function(e) {
                var id = $(this).data("id");
                $.ajax({
                    type: 'POST',
                    url: `{{ route('checklists.remove') }}`,
                    data: {_token: '{{ csrf_token() }}', 'board_id': {{ $board->id }}, 'card_id': id},
                    success: function(data) {
                        if(data) {
                            $(".removeChecklists").remove();
                            $("#checklists").empty();
                        }
                    },
                    error: function(data) {
                        if(data.status == 401 || data.status == 403) {
                            let url = "{{ route('home') }}";
                            document.location.href = url;
                        }
                    }
                });
            });
        @endcan

        $(document).on("mouseenter", ".card", function() {
            $(this).css('cursor', 'pointer');
        });

        $(document).on("mouseleave", ".card", function() {
            $(this).css('cursor', 'default');
        });

        $(document).on('click', '.card-modal .dropdown-menu', function (e) {
            e.stopPropagation();
        });

        @can('update', $board)
            function labelColorSelection(event, element) {
                event.preventDefault();
                $(".card-modal .dropdown-menu .label-background-select").each(function() {
                    $(this).text("");
                });
                element.innerHTML = "<i class='fa fa-check text-white' aria-hidden='true'></i>";
                var color = RGBToHex(element.style.background);
                $('.card-modal #create-label').attr('data-color', color);
                $('.card-modal #create-label').data('color', color);
            }

            $("#create-label").click(function() {
                var id = $(this).data("id");
                var name = $("#label-name").val();
                if(name == "") {
                    $(".label_error").html("Please give a name to label!");
                    return;
                }
                var color = $(this).data("color");
                if(color == '') {
                    $(".label_error").html("Please select a color for label!");
                    return;
                }
                $(".label_error").html('');
                $.ajax({
                    type: 'POST',
                    url: `{{ route('labels.store') }}`,
                    data: {_token: '{{ csrf_token() }}', 'board_id': {{ $board->id }}, 'card_id': id, 'name': name, 'color': color },
                    success: function(data) {
                        if(data) {
                            $(".card-modal .dropdown-toggle").dropdown('toggle');
                            $("#labels").append(`<span class="badge text-white" id="${data.result}" style="background: ${color}">${name}</span>`);
                            $(`.card#${id}`).find('.card-labels').append(`<span class="badge text-white" id="${data.result}" style="background: ${color}">${name}</span>`);
                        }
                    },
                    error: function(data) {
                        if(data.status == 401 || data.status == 403) {
                            let url = "{{ route('home') }}";
                            document.location.href = url;
                        }
                    }
                });
            });

            $("#labels").on('click', '.badge', function() {
                var id = $(this).attr("id");
                var deleteConfirm = confirm("Do you want to delete this label?");
                if(deleteConfirm == true) {
                    $.ajax({
                    type: 'POST',
                    url: `{{ route('labels.destroy') }}`,
                    data: {_token: '{{ csrf_token() }}', 'board_id': {{ $board->id }}, 'label_id': id },
                    success: function(data) {
                        if(data) {
                            $(`#labels .badge#${id}`).remove();
                            $(`.card-labels .badge#${id}`).remove();
                        }
                    },
                    error: function(data) {
                        if(data.status == 401 || data.status == 403) {
                            let url = "{{ route('home') }}";
                            document.location.href = url;
                        }
                    }
                });
                }
            });
        @endcan
    </script>
@endsection

@section('broadcasting-scripts')
    @can('hasBoard', $board)
        <script>
            var id = {{ $board->id }};
            var auth = {{ auth()->id() }};
            Echo.private(`board-update.${id}`)
                .listen('BoardUpdateEvent', (e) => {
                    if(auth != e.board.auth) {
                        $("#title").val(e.board.title);
                        $("body").css("background-color", e.board.background);
                        var navbar_color = "";
                        if(e.board.background == "#0079bf") {
                            navbar_color = '{{ $navbar_backgrounds[0] }}';
                        } else if(e.board.background == "#d29034") {
                            navbar_color = '{{ $navbar_backgrounds[1] }}';
                        } else if(e.board.background == "#519839") {
                            navbar_color = '{{ $navbar_backgrounds[2] }}';
                        } else if(e.board.background == "#b04632") {
                            navbar_color = '{{ $navbar_backgrounds[3] }}';
                        } else if(e.board.background == "#89609e") {
                            navbar_color = '{{ $navbar_backgrounds[4] }}';
                        } else if(e.board.background == "#cd5a91") {
                            navbar_color = '{{ $navbar_backgrounds[5] }}';
                        } else if(e.board.background == "#4bbf6b") {
                            navbar_color = '{{ $navbar_backgrounds[6] }}';
                        } else if(e.board.background == "#00aecc") {
                            navbar_color = '{{ $navbar_backgrounds[7] }}';
                        } else if(e.board.background == "#838c91") {
                            navbar_color = '{{ $navbar_backgrounds[8] }}';
                        }
                        $(".background-primary").css('background-color', navbar_color);
                    }
                });

                Echo.private(`list.${id}`)
                .listen('ListEvent', (e) => {
                    if(auth != e.list.auth) {
                        $(".create-list-button").before(`
                            <div class="list p-2 rounded" style="background: #ebecf0; width: 250px; flex-shrink: 0;" data-id="${e.list.list_id}">
                                <input type="text" name="list_name" class="form-control form-control-sm mb-2 list-name" value="${e.list.name}" data-id="${e.list.list_id}">
                                <div class="cards sortable ui-sortable" style="min-height: 10px;" data-id="${e.list.list_id}">
                                </div>
                                <button class="create-card p-2 btn btn-light btn-sm text-muted" id="add-card-${e.list.list_id}">
                                    <i class="fa fa-plus mr-2"></i>
                                    Add a card
                                </button>
                                <div class="add-card d-none">
                                    <input type="text" class="form-control" name="card" placeholder="Enter a title for this card..." autocomplete="off">
                                    <button class="btn btn-primary mt-1 btn-sm add-card-button" id="add-card-to-list-${e.list.list_id}" data-id="${e.list.list_id}">Add card</button>
                                    <button class="btn mt-1 btn-sm card-cross" style="background: transparent;" id="cross-card-${e.list.list_id}">
                                        <i class="fa fa-times" aria-hidden="true"></i>
                                    </button>
                                </div>
                            </div>
                        `);
                    }
                });

                Echo.private(`list-update.${id}`)
                .listen('ListUpdateEvent', (e) => {
                    if(auth != e.list.auth) {
                        $(`.list-name[data-id='${e.list.list_id}'`).val(e.list.name);
                    }
                });

                Echo.private(`card.${id}`)
                .listen('CardEvent', (e) => {
                    if(auth != e.card.auth) {
                        $(`.cards[data-id='${e.card.list_id}']`).append(`
                            <div class="card mb-2 ui-sortable-handle" id="${e.card.card_id}" data-id="${e.card.card_id}">
                                <div class="card-body p-2">
                                    <div class="card-labels"></div>
                                    <p class="m-0">${e.card.name}</p>
                                </div>
                            </div>
                        `);
                    }
                });

                Echo.private(`card-update.${id}`)
                .listen('CardUpdateEvent', (e) => {
                    if(auth != e.card.auth) {
                        if(e.card.feature == 'listChange') {
                            $(`.card[data-id='${e.card.card_id}']`).remove();
                            $(`.cards[data-id='${e.card.list_id}']`).append(`
                                <div class="card mb-2 ui-sortable-handle" id="${e.card.card_id}" data-id="${e.card.card_id}">
                                    <div class="card-body p-2">
                                        <div class="card-labels"></div>
                                        <p class="m-0">${e.card.name}</p>
                                    </div>
                                </div>
                            `);
                        } else if(e.card.feature == 'nameChange') {
                            $(`.card[data-id='${e.card.card_id}']`).find('p').text(e.card.name);
                        } else if(e.card.feature == 'createLabel') {
                            $(`.card[data-id='${e.card.card_id}']`).find('.card-labels').append(`
                                <span class="badge text-white" id="${e.card.label_id}" style="background: ${e.card.label_color}">${e.card.label_name}</span>
                            `);
                        } else if(e.card.feature == 'removeLabel') {
                            $(`.badge.text-white#${e.card.label_id}`).remove();
                        }
                    }
                });
        </script>
    @endcan
@endsection
