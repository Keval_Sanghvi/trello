<div class="modal fade create-board-modal" tabindex="-1" role="dialog" aria-hidden="true" style="margin-left: 46%; transform: translateX(-50%);">
    <div class="modal-dialog modal-sm" role="document" data-backdrop="static">
        <div class="modal-content" style="background: transparent; border: 0;">
            <form action="{{ route('boards.store') }}" method="POST" autocomplete="off" id="create-board-form">
                @csrf
                <input type="hidden" name="background" id="background-attribute" value="#0079bf">
                <div style="position: absolute; left: 320px; height: 120px" id="backgrounds">
                    @foreach($board_backgrounds as $board_background)
                        <button class="board-background-select" style="border: 0; background: {{ $board_background }}; width: 30px; height: 30px; margin: 5px;
                            @if($loop->index == 3 || $loop->index == 4 || $loop->index == 5) position: relative; left: 40px; bottom: 120px @endif
                            @if($loop->index == 6 || $loop->index == 7 || $loop->index == 8) position: relative; left: 80px; bottom: 240px @endif"
                            onClick = "backgroundSelection(event, this)"
                        >
                        @if($loop->index == 0)
                            <i class='fa fa-check text-white' aria-hidden='true'></i>
                        @endif
                        </button>
                    @endforeach
                </div>
                <div class="modal-body" style="background: #0079bf;" id="board-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group d-inline-block">
                                <input
                                    type="text"
                                    class="form-control form-control-sm"
                                    id="title"
                                    name="title"
                                    value="{{ old('title') }}"
                                    placeholder="Add board title"
                                    style="background: transparent; color: white;">
                            </div>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>

                    @if(auth()->user()->workspaces()->count() != 0)
                        <select class="form-control form-control-sm" id="select-workspace" name="workspace_id" style="background: transparent; color: white;">
                            @foreach($workspaces as $workspace)
                                @if($workspace->name == auth()->user()->default_workspace_name)
                                    <option selected value="{{ $workspace->id }}" style="color: black;">{{ $workspace->name }}</option>
                                @else
                                    <option value="{{ $workspace->id }}" style="color: black;">{{ $workspace->name }}</option>
                                @endif
                            @endforeach
                        </select>
                    @endif

                    <select class="form-control form-control-sm mt-3 selectpicker" name="visibility">
                        @foreach($board_visibility_types as $board_visibility_type)
                            @if($board_visibility_type == 'workspace' && auth()->user()->workspaces()->count() != 0)
                                <option selected data-icon="fa fa-users mr-1" data-subtext="All members of this Workspace can see and edit this board." value="{{ $board_visibility_type }}">{{ Str::ucfirst($board_visibility_type) }}</option>
                            @elseif($board_visibility_type == 'public')
                                <option data-icon="fa fa-globe mr-1" data-subtext="Anyone on the internet (including Google) can see this board. Only board members can edit." value="{{ $board_visibility_type}}">{{ Str::ucfirst($board_visibility_type) }}</option>
                            @elseif($board_visibility_type == 'private')
                                <option data-icon="fa fa-lock mr-1" data-subtext="Only board members can see and edit this board." value="{{ $board_visibility_type}}">{{ Str::ucfirst($board_visibility_type) }}</option>
                            @endif
                        @endforeach
                    </select>

                </div>
                <div class="modal-footer justify-content-start" style="border: 0; padding: 0">
                    <button type="submit" id="create-board-submit" class="btn btn-sm btn-light text-muted mt-3" style="cursor: not-allowed;">Create Board</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#create-board-submit').prop('disabled', true);
        $('#title').keyup(function() {
            if($('#title').val() == ""){
                $('#create-board-submit').attr('disabled', 'disabled');
                $('#create-board-submit')[0].style.cursor = "not-allowed";
                $('#create-board-submit').removeClass();
                $('#create-board-submit').addClass("btn btn-sm btn-light text-muted mt-3");
            }
            else{
                $('#create-board-submit').removeAttr('disabled');
                $('#create-board-submit')[0].style.cursor = "pointer";
                $('#create-board-submit').removeClass();
                $('#create-board-submit').addClass("btn btn-sm background-primary text-white mt-3");
            }
        });
    });
</script>

<script src="{{ asset('js/boards/create-validation.js') }}"></script>

<script>
    function backgroundSelection(event, element) {
        event.preventDefault();
        $("#backgrounds button").each(function() {
            $(this).text("");
        });
        element.innerHTML = "<i class='fa fa-check text-white' aria-hidden='true'></i>";
        let color = RGBToHex(element.style.background);
        $("#board-body").css('background', color);
        $(".bootstrap-select .dropdown-toggle").css('background', color);
        $("#background-attribute").val(color);
    }
</script>

<script>
    @error('title')
        $('.create-board-modal').modal('show');
    @enderror
    @error('background')
        $('.create-board-modal').modal('show');
    @enderror
    @error('visibility')
        $('.create-board-modal').modal('show');
    @enderror
    @error('workspace_id')
        $('.create-board-modal').modal('show');
    @enderror
</script>
