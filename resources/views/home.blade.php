@extends('layouts.app')

@section('page-level-styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha512-SfTiTlX6kk+qitfevl/7LibUOeJWlt9rbyDn92a1DqWOw9vWG2MFoays0sgObmWazO5BQPiFucnnEAjpAB+/Sw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
@endsection

@section('main-content')
    <div class="container">
        <div class="row mt-4 mb-4">
           <div class="col-md-12">
                @include('layouts.partials._message')
           </div>
        </div>
        <div class="row mt-5 mb-5">

            @include('layouts.partials._sidebar')

            <div class="col-md-6">
                <div class="card">
                    <img class="card-img-top" src="{{ asset('images/home-page.svg') }}" alt="Home Image">
                    <div class="card-body">
                        <p class="text-center font-weight-bold">Stay on track and up to date</p>
                        <p class="card-text text-center small">Invite people to boards and cards, leave comments, add due dates, and we'll show the most important activity here.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3" id="feature">
                @if(! $recents->isEmpty())
                    <div class="card">
                        <div class="card-header">
                            <i class="fa fa-clock-o mr-2" aria-hidden="true"></i>Recently Viewed
                        </div>
                        <div class="list-group" id="recent-list" role="tablist">
                            @foreach($recents as $recent)
                                <a class="list-group-item list-group-item-action" href="{{ route('boards.show', $recent->id) }}">
                                    {{ $recent->title }}
                                    <i class="fa @if($recent->isFavourite() == 1) fa-star @else fa-star-o @endif text-warning float-right star" style="width: 50px; margin-top: 6px; text-align: center; margin-right: -10px" data-star="@if($recent->isFavourite() == 1) y @else n @endif" data-feature="recents" aria-hidden="true" data-id="{{ $recent->id }}"></i>
                                </a>
                            @endforeach
                        </div>
                    </div>
                @endif
                @if(! $favourites->isEmpty())
                    <div class="card mt-5 favourites">
                        <div class="card-header">
                            <i class="fa fa-clock-o mr-2" aria-hidden="true"></i>Starred
                        </div>
                        <div class="list-group" id="favourite-list" role="tablist">
                            @foreach($favourites as $favourite)
                                <a class="list-group-item list-group-item-action" href="{{ route('boards.show', $favourite->id) }}">
                                    {{ $favourite->title }}
                                    <i class="fa fa-star text-warning float-right star" style="width: 50px; margin-top: 6px; text-align: center; margin-right: -10px" aria-hidden="true" data-feature="favourites" data-star="y" data-id="{{ $favourite->id }}"></i>
                                </a>
                            @endforeach
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection

@section('page-level-scripts')
    <script>
        $("#feature").on('click', '.star', function(e) {
            e.preventDefault();
            let id = $(this).data("id");
            let mark = $(this).data("star");
            let feature = $(this).data("feature");
            $.ajax({
                type: 'POST',
                url: `{{ route('boards.changeFavouriteStatus') }}`,
                data: {_token: '{{ csrf_token() }}', 'board_id': id, 'mark': mark },
                success: function(data) {
                    if(data) {
                        if(feature == "favourites") {
                            $(`#favourite-list [data-id=${id}]`).parent().remove();
                            if($("#favourite-list a").length == 0) {
                                $(".card.favourites").remove();
                            }
                            $(`#recent-list [data-id=${id}]`).attr("data-star", "n");
                            $(`#recent-list [data-id=${id}]`).removeClass('fa-star');
                            $(`#recent-list [data-id=${id}]`).addClass('fa-star-o');
                        } else if(feature == "recents") {
                            if(mark.includes("n")) {
                                $(`#recent-list [data-id=${id}]`).attr("data-star", "y");
                                $(`#recent-list [data-id=${id}]`).data("star", "y");
                                let title = $(`#recent-list [data-id=${id}]`).parent().text().trim();
                                $(`#recent-list [data-id=${id}]`).removeClass("fa-star-o");
                                $(`#recent-list [data-id=${id}]`).addClass("fa-star");
                                if($("#feature .card").length == 1) {
                                    $("#feature").append(`
                                        <div class="card mt-5 favourites">
                                            <div class="card-header">
                                                <i class="fa fa-clock-o mr-2" aria-hidden="true"></i>Starred
                                            </div>
                                            <div class="list-group" id="favourite-list" role="tablist">
                                                <a class="list-group-item list-group-item-action" href="/boards/${id}">
                                                    ${title}
                                                    <i class="fa fa-star text-warning float-right star" style="width: 50px; margin-top: 6px; text-align: center; margin-right: -10px" aria-hidden="true" data-feature="favourites" data-star="y" data-id="${id}"></i>
                                                </a>
                                            </div>
                                        </div>
                                    `);
                                } else {
                                    if($(`#favourite-list [data-id=${id}]`).length == 0) {
                                        $("#favourite-list").append(`
                                            <a class="list-group-item list-group-item-action" href="/boards/${id}">
                                                ${title}
                                                <i class="fa fa-star text-warning float-right star" style="width: 50px; margin-top: 6px; text-align: center; margin-right: -10px" aria-hidden="true" data-feature="favourites" data-star="y" data-id="${id}"></i>
                                            </a>
                                        `);
                                    }
                                }
                            } else {
                                $(`#recent-list [data-id=${id}]`).attr("data-star", "n");
                                $(`#recent-list [data-id=${id}]`).data("star", "n");
                                $(`#recent-list [data-id=${id}]`).removeClass("fa-star");
                                $(`#recent-list [data-id=${id}]`).addClass("fa-star-o");
                                $(`#favourite-list [data-id=${id}]`).parent().remove();
                                if($("#favourite-list a").length == 0) {
                                    $(".card.favourites").remove();
                                }
                            }
                        }
                    }
                },
                error: function(data) {
                    if(data.status == 401 || data.status == 403) {
                        let url = "{{ route('home') }}";
                        document.location.href = url;
                    }
                }
            });
        });
    </script>
@endsection

@section('broadcasting-scripts')
    <script>
        var workspaces = {{ json_encode(auth()->user()->workspaces->pluck('id')->toArray()) }};
        for(var i = 0; i < workspaces.length; i++) {
            Echo.private(`workspace-update.${workspaces[i]}`)
                .listen('WorkspaceUpdateEvent', (e) => {
                    let workspace_id = e.workspace.workspace_id;
                    let workspace_name = e.workspace.name;
                    $(`#workspaces #workspace-${workspace_id}`).html(`
                        <span style="background: linear-gradient(#cc4223, #cb7d25); padding: .3rem .7rem;" class="rounded text-white mr-2">${workspace_name[0]}</span>
                            ${workspace_name}
                        <i class="fa fa-caret-down ml-2" aria-hidden="true"></i>
                    `);
                });
        }

        var auth = {{ auth()->id() }};
        Echo.private(`workspace.${auth}`)
            .listen('WorkspaceEvent', (e) => {
                let workspace_id = e.workspace.workspace_id;
                let workspace_name = e.workspace.name;
                if(e.workspace.feature == "add") {
                    $("#workspaces").append(`
                        <div>
                            <a class="deco-none d-block mt-3" id="workspace-${workspace_id}" data-toggle="collapse" style="font-size: 15px;" href="#collapse-${workspace_id}" role="button" aria-expanded="false" aria-controls="collapseExample">
                                <span style="background: linear-gradient(#cc4223, #cb7d25); padding: .3rem .7rem;" class="rounded text-white mr-2">${workspace_name[0]}</span>
                                    ${workspace_name}
                                <i class="fa fa-caret-down ml-2" aria-hidden="true"></i>
                            </a>
                            <div class="collapse mt-2" id="collapse-${workspace_id}">
                                <div class="list-group">
                                    <a href="/workspaces/${workspace_id}" class="list-group-item list-group-item-action">Workspace</a>
                                    <a href="/workspaces/${workspace_id}/boards" class="list-group-item list-group-item-action">Boards</a>
                                    <a href="/workspaces/${workspace_id}/members" class="list-group-item list-group-item-action">Members</a>
                                </div>
                            </div>
                        </div>
                    `);
                } else if(e.workspace.feature == "remove" || e.workspace.feature == "destroy") {
                    $(`#workspaces #workspace-${workspace_id}`).remove();
                }
            });

        var boards = {{ json_encode(auth()->user()->boards->pluck('id')->toArray()) }};
        for(var i = 0; i < boards.length; i++) {
            Echo.private(`board-update.${boards[i]}`)
                .listen('BoardUpdateEvent', (e) => {
                    if(auth != e.board.auth) {
                        $(`#recent-list [data-id=${e.board.id}]`).parent().contents().first()[0].textContent = e.board.title;
                        $(`#favourite-list [data-id=${e.board.id}]`).parent().contents().first()[0].textContent = e.board.title;
                    }
                });
        }
    </script>
@endsection
